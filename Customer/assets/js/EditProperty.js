"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/realestates";
const gBASE_URL = "http://localhost:8080/";
/*** REGION 1 - Global variables */
var gId = new URLSearchParams(window.location.search).get("id");
var gRealesateId = new URLSearchParams(window.location.search).get("realestateId");
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
var gDistrictId;
var gProvinceId;
var gWardId;
var gStreetId;
var gProjectId;
var gPhoto;
/*** REGION 2 - Elements' events declaration */
// load selected photo
$(":file").change(function () {
	var src = URL.createObjectURL(this.files[0]);
	$("#img").attr("src", src);
	$("#show-photo").show();
});

//on btn delete photo
$("#btn-delete").on("click", function () {
	$("#img").attr("src", "");
	$("#show-photo").hide();
	$("#inp-photo").val("");
});

// on image can't load picture
$("#img").on("error", function () {
	$("#img").attr("src", "");
	$("#show-photo").hide();
	$("#inp-photo").val("");
});
//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});
// on btn save is clicked
$("#btn-save").on("click", function () {
	// get data
	var vDataObj = getData();
	// validate data
	var vValid = validateData(vDataObj);
	if (vValid) {
		// add data obj and file to form data
		var vFormData = new FormData();
		Object.entries(vDataObj).forEach(([key, value]) => {
			vFormData.append(key, value);
		});
		if ($("#inp-photo")[0].files.length > 0) {
			vFormData.append("image", $("#inp-photo")[0].files[0]);
		} else {
			vFormData.append(
				"image",
				new Blob(),
				$("#img")
					.attr("src")
					.substring($("#img").attr("src").lastIndexOf("/") + 1)
			);
		}
		// add data
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "PUT",
			url: gURL + "/" + gRealesateId,
			data: vFormData,
			contentType: false,
			processData: false,
			success: function (response) {
				window.location.href = "MyProperties.html?id=" + gId;
			},
			error: function (error) {
				toastr.error("Edit failed");
				console.log(error);
			},
		});
	}
});

$(document).ready(function () {
	// load data to page
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gURL + "/" + gRealesateId,
		success: function (response) {
			loadDataToPage(response);
		},
		error: function (error) {
			loadErrorModal("Load data failed");
			$("#modal-error").on("hidden.bs.modal", function () {
				history.back();
			});
		},
	});
	//load provinces to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "provinces",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-province").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
			$("#sel-province").val(gProvinceId).trigger("change");
		},
	});
});

//when a province is chosen load district
$("#sel-province").change(function () {
	var vProvinceId = $("#sel-province").val();
	$("#sel-district").val("").trigger("change");
	$("#sel-district :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-district").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gDistrictId) {
					$("#sel-district").val(gDistrictId).trigger("change");
					gDistrictId = null;
				}
			});
		},
	});
});

//when a district is chosen load ward
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-ward").val("").trigger("change");
	$("#sel-ward :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/wards",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-ward").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gWardId) {
					$("#sel-ward").val(gWardId).trigger("change");
					gWardId = null;
				}
			});
		},
	});
});

//when a district is chosen load street
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-street").val("").trigger("change");
	$("#sel-street :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/streets",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-street").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gStreetId) {
					$("#sel-street").val(gStreetId).trigger("change");
					gStreetId = null;
				}
			});
		},
	});
});

//when a district is chosen load project
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-project").val("").trigger("change");
	$("#sel-project :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/projects",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-project").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);

				if (data.id == gProjectId) {
					$("#sel-project").val(gProjectId).trigger("change");
					gProjectId = null;
				}
			});
		},
	});
});

/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//load data to page
//input: data obj
function loadDataToPage(paramDataObj) {
	$("#inp-address").val(paramDataObj.address);
	$("#img").attr("src", gBASE_URL + "uploads/" + paramDataObj.photo);
	gPhoto = paramDataObj.photo;
	$("#show-photo").show();
	gProvinceId = paramDataObj.provinceId;
	$("#sel-province").val(gDistrictId).trigger("change");
	gDistrictId = paramDataObj.districtId;
	$("#sel-district").val(gDistrictId).trigger("change");
	gWardId = paramDataObj.wardsId;
	$("#sel-ward").val(gWardId).trigger("change");
	gStreetId = paramDataObj.streetId;
	$("#sel-street").val(gStreetId).trigger("change");
	gProjectId = paramDataObj.projectId;
	$("#sel-project").val(gProjectId).trigger("change");
	$("#inp-title").val(paramDataObj.title);
	$("#sel-type").val(paramDataObj.type).trigger("change");
	$("#sel-request").val(paramDataObj.request).trigger("change");
	$("#inp-price").val(paramDataObj.price);
	$("#inp-price-min").val(paramDataObj.priceMin);
	$("#sel-price-time").val(paramDataObj.priceTime).trigger("change");
	$("#inp-apart-code").val(paramDataObj.apartCode);
	$("#inp-wall-area").val(paramDataObj.wallArea);
	$("#inp-bedroom").val(paramDataObj.bedroom);
	$("#sel-balcony").val(paramDataObj.balcony).trigger("change");
	$("#inp-landscape-view").val(paramDataObj.landscapeView);
	$("#sel-apart-loca").val(paramDataObj.apartLoca).trigger("change");
	$("#sel-apart-type").val(paramDataObj.apartType).trigger("change");
	$("#sel-furniture-type").val(paramDataObj.furnitureType).trigger("change");
	$("#inp-price-rent").val(paramDataObj.priceRent);
	$("#inp-return-rate").val(paramDataObj.returnRate);
	$("#inp-acreage").val(paramDataObj.acreage);
	$("#sel-direction").val(paramDataObj.direction).trigger("change");
	$("#inp-total-floors").val(paramDataObj.totalFloors);
	$("#inp-number-floors").val(paramDataObj.numberFloors);
	$("#inp-bath").val(paramDataObj.bath);
	$("#inp-legal-doc").val(paramDataObj.legalDoc);
	$("#inp-description").val(paramDataObj.description);
	$("#inp-width-y").val(paramDataObj.widthY);
	$("#inp-long-x").val(paramDataObj.longX);
	if (paramDataObj.streetHouse == 1) {
		$("#inp-street-house").prop("checked", true);
	} else {
		$("#inp-street-house").prop("checked", false);
	}
	if (paramDataObj.fsbo == 1) {
		$("#inp-fsbo").prop("checked", true);
	} else {
		$("#inp-fsbo").prop("checked", false);
	}
	$("#inp-view-number").val(paramDataObj.viewNum);
	$("#inp-shape").val(paramDataObj.shape);
	$("#inp-distance-2-facade").val(paramDataObj.distance2Facade);
	$("#inp-adjacent-facade-num").val(paramDataObj.adjacentFacadeNum);
	$("#inp-adjacent-road").val(paramDataObj.adjacentRoad);
	$("#inp-ctxd-price").val(paramDataObj.ctxdPrice);
	$("#inp-ctxd-value").val(paramDataObj.ctxdValue);
	$("#inp-alley-min-width").val(paramDataObj.alleyMinWidth);
	$("#inp-adjacent-alley-min-width").val(paramDataObj.adjacentAlleyMinWidth);
	$("#inp-factor").val(paramDataObj.factor);
	$("#inp-structure").val(paramDataObj.structure);
	$("#inp-dtsxd").val(paramDataObj.dtsxd);
	$("#inp-clcl").val(paramDataObj.clcl);
	$("#inp-latitude").val(paramDataObj.latitude);
	$("#inp-longtitude").val(paramDataObj.longtitude);
}

// get data
// output: data obj
function getData() {
	var vDataObj = {
		address: $("#inp-address").val().trim(),
		photo: gPhoto,
		provinceId: $("#sel-province").val(),
		districtId: $("#sel-district").val(),
		wardsId: $("#sel-ward").val(),
		streetId: $("#sel-street").val(),
		projectId: $("#sel-project").val(),
		title: $("#inp-title").val().trim(),
		type: $("#sel-type").val(),
		request: $("#sel-request").val(),
		customerId: gId,
		price: $("#inp-price").val().trim(),
		priceMin: $("#inp-price-min").val().trim(),
		priceTime: $("#sel-price-time").val(),
		apartCode: $("#inp-apart-code").val().trim(),
		wallArea: $("#inp-wall-area").val().trim(),
		bedroom: $("#inp-bedroom").val().trim(),
		balcony: $("#sel-balcony").val(),
		landscapeView: $("#inp-landscape-view").val().trim(),
		apartLoca: $("#sel-apart-loca").val(),
		apartType: $("#sel-apart-type").val(),
		furnitureType: $("#sel-furniture-type").val(),
		priceRent: $("#inp-price-rent").val().trim(),
		returnRate: $("#inp-return-rate").val().trim(),
		acreage: $("#inp-acreage").val().trim(),
		direction: $("#sel-direction").val(),
		totalFloors: $("#inp-total-floors").val().trim(),
		numberFloors: $("#inp-number-floors").val().trim(),
		bath: $("#inp-bath").val().trim(),
		legalDoc: $("#inp-legal-doc").val().trim(),
		description: $("#inp-description").val().trim(),
		widthY: $("#inp-width-y").val().trim(),
		longX: $("#inp-long-x").val().trim(),
		streetHouse: "",
		fsbo: "",
		viewNum: $("#inp-view-number").val().trim(),
		shape: $("#inp-shape").val().trim(),
		distance2Facade: $("#inp-distance-2-facade").val().trim(),
		adjacentFacadeNum: $("#inp-adjacent-facade-num").val().trim(),
		adjacentRoad: $("#inp-adjacent-road").val().trim(),
		ctxdPrice: $("#inp-ctxd-price").val().trim(),
		ctxdValue: $("#inp-ctxd-value").val().trim(),
		alleyMinWidth: $("#inp-alley-min-width").val().trim(),
		adjacentAlleyMinWidth: $("#inp-adjacent-alley-min-width").val().trim(),
		factor: $("#inp-factor").val().trim(),
		structure: $("#inp-structure").val().trim(),
		dtsxd: $("#inp-dtsxd").val().trim(),
		clcl: $("#inp-clcl").val().trim(),
		latitude: $("#inp-latitude").val().trim(),
		longtitude: $("#inp-longtitude").val().trim(),
		approved: "0",
	};
	if ($("#inp-street-house").is(":checked")) {
		vDataObj.streetHouse = 1;
	} else {
		vDataObj.streetHouse = 0;
	}
	if ($("#inp-fsbo").is(":checked")) {
		vDataObj.fsbo = 1;
	} else {
		vDataObj.fsbo = 0;
	}

	return vDataObj;
}

//validate data
//input: data obj
//output: data obj
function validateData(paramDataObj) {
	if (paramDataObj.address == "") {
		loadErrorModal("Please enter Address");
		return false;
	}
	if (isNaN(paramDataObj.price)) {
		loadErrorModal("Please enter a Price as a number");
		return false;
	}
	if (isNaN(paramDataObj.priceMin)) {
		loadErrorModal("Please enter a Price Min as a number");
		return false;
	}
	if (isNaN(paramDataObj.wallArea)) {
		loadErrorModal("Please enter Wall Area as a number");
		return false;
	}

	if (isNaN(paramDataObj.bedroom)) {
		loadErrorModal("Please enter Bedroom as a number");
		return false;
	}
	if (isNaN(paramDataObj.priceRent)) {
		loadErrorModal("Please enter Price Rent as a number");
		return false;
	}
	if (isNaN(paramDataObj.returnRate)) {
		loadErrorModal("Please enter Return rate as a number");
		return false;
	}
	if (isNaN(paramDataObj.acreage)) {
		loadErrorModal("Please enter Acreage as a number");
		return false;
	}
	if (isNaN(paramDataObj.totalFloors)) {
		loadErrorModal("Please enter Total Floors as a number");
		return false;
	}
	if (isNaN(paramDataObj.numberFloors)) {
		loadErrorModal("Please enter Number of floors as a number");
		return false;
	}
	if (isNaN(paramDataObj.bath)) {
		loadErrorModal("Please enter Bath as a number");
		return false;
	}
	if (isNaN(paramDataObj.legalDoc)) {
		loadErrorModal("Please enter Legal Doc as a number");
		return false;
	}
	if (isNaN(paramDataObj.widthY)) {
		loadErrorModal("Please enter Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.longX)) {
		loadErrorModal("Please enter Long as a number");
		return false;
	}
	if (isNaN(paramDataObj.viewNum)) {
		loadErrorModal("Please enter View Number as a number");
		return false;
	}
	if (isNaN(paramDataObj.distance2Facade)) {
		loadErrorModal("Please enter Distance to Facade as a number");
		return false;
	}
	if (isNaN(paramDataObj.adjacentFacadeNum)) {
		loadErrorModal("Please enter Adjacent Facade Num as a number");
		return false;
	}
	if (isNaN(paramDataObj.ctxdPrice)) {
		loadErrorModal("Please enter CTXD Price as a number");
		return false;
	}
	if (isNaN(paramDataObj.ctxdValue)) {
		loadErrorModal("Please enter CTXD Value as a number");
		return false;
	}

	if (isNaN(paramDataObj.alleyMinWidth)) {
		loadErrorModal("Please enter Alley Min Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.adjacentAlleyMinWidth)) {
		loadErrorModal("Please enter Adjacent Alley Min Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.dtsxd)) {
		loadErrorModal("Please enter DTSXD as a number");
		return false;
	}

	if (isNaN(paramDataObj.clcl)) {
		loadErrorModal("Please enter CLCL as a number");
		return false;
	}
	if (isNaN(paramDataObj.latitude)) {
		loadErrorModal("Please enter Latitude as a number");
		return false;
	}
	if (isNaN(paramDataObj.longtitude)) {
		loadErrorModal("Please enter Longtitude as a number");
		return false;
	}

	return true;
}

//load error modal
//input: error message
function loadErrorModal(paramErrorMessage) {
	$("#p-error").text(paramErrorMessage);
	$("#modal-error").modal("show");
}
