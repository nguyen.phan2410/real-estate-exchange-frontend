"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/realestates";
const gBASE_URL = "http://localhost:8080/";
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
var gId = new URLSearchParams(window.location.search).get("id");
/*** REGION 1 - Global variables */
/*** REGION 2 - Elements' events declaration */
// load selected photo
$(":file").change(function () {
	var src = URL.createObjectURL(this.files[0]);
	$("#img").attr("src", src);
	$("#show-photo").show();
});

//on btn delete photo
$("#btn-delete").on("click", function () {
	$("#img").attr("src", "");
	$("#show-photo").hide();
	$("#inp-photo").val("");
});

//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});

// when page load
$(document).ready(function () {
	//load provinces to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "provinces",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-province").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a province is chosen load districts
$("#sel-province").change(function () {
	var vProvinceId = $("#sel-province").val();
	$("#sel-district").val("").trigger("change");
	$("#sel-district :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-district").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load ward
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-ward").val("").trigger("change");
	$("#sel-ward :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/wards",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-ward").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load street
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-street").val("").trigger("change");
	$("#sel-street :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/streets",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-street").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load project
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-project").val("").trigger("change");
	$("#sel-project :not(:first-child)").remove();
	// load district to select
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/projects",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-project").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});

//on btn save is clicked
$("#btn-save").on("click", function () {
	// get data
	var vDataObj = getData();

	// validate data
	var vValid = validateData(vDataObj);
	if (vValid) {
		// create form data with vDataObj and File
		var vFormData = new FormData();
		Object.entries(vDataObj).forEach(([key, value]) => {
			vFormData.append(key, value);
		});
		if ($("#inp-photo")[0].files.length > 0) {
			vFormData.append("image", $("#inp-photo")[0].files[0]);
		} else {
			vFormData.append("image", new Blob());
		}

		// add data
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "POST",
			url: gURL,
			data: vFormData,
			contentType: false,
			processData: false,
			success: function (response) {
				window.location.href = "MyProperties.html?id=" + gId;
			},
			error: function (error) {
				toastr.error("Add failed");
				console.log(error);
			},
		});
	}
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
// get data
// output: data obj
function getData() {
	var vDataObj = {
		address: $("#inp-address").val().trim(),
		photo: "",
		provinceId: $("#sel-province").val().trim(),
		districtId: $("#sel-district").val().trim(),
		wardsId: $("#sel-ward").val().trim(),
		streetId: $("#sel-street").val().trim(),
		projectId: $("#sel-project").val().trim(),
		title: $("#inp-title").val().trim(),
		type: $("#sel-type").val().trim(),
		request: $("#sel-request").val().trim(),
		customerId: gId,
		price: $("#inp-price").val().trim(),
		priceMin: $("#inp-price-min").val().trim(),
		priceTime: $("#sel-price-time").val().trim(),
		apartCode: $("#inp-apart-code").val().trim(),
		wallArea: $("#inp-wall-area").val().trim(),
		bedroom: $("#inp-bedroom").val().trim(),
		balcony: $("#sel-balcony").val().trim(),
		landscapeView: $("#inp-landscape-view").val().trim(),
		apartLoca: $("#sel-apart-loca").val().trim(),
		apartType: $("#sel-apart-type").val().trim(),
		furnitureType: $("#sel-furniture-type").val().trim(),
		priceRent: $("#inp-price-rent").val().trim(),
		returnRate: $("#inp-return-rate").val().trim(),
		acreage: $("#inp-acreage").val().trim(),
		direction: $("#sel-direction").val().trim(),
		totalFloors: $("#inp-total-floors").val().trim(),
		numberFloors: $("#inp-number-floors").val().trim(),
		bath: $("#inp-bath").val().trim(),
		legalDoc: $("#inp-legal-doc").val().trim(),
		description: $("#inp-description").val().trim(),
		widthY: $("#inp-width-y").val().trim(),
		longX: $("#inp-long-x").val().trim(),
		streetHouse: "",
		fsbo: "",
		viewNum: $("#inp-view-number").val().trim(),
		shape: $("#inp-shape").val().trim(),
		distance2Facade: $("#inp-distance-2-facade").val().trim(),
		adjacentFacadeNum: $("#inp-adjacent-facade-num").val().trim(),
		adjacentRoad: $("#inp-adjacent-road").val().trim(),
		ctxdPrice: $("#inp-ctxd-price").val().trim(),
		ctxdValue: $("#inp-ctxd-value").val().trim(),
		alleyMinWidth: $("#inp-alley-min-width").val().trim(),
		adjacentAlleyMinWidth: $("#inp-adjacent-alley-min-width").val().trim(),
		factor: $("#inp-factor").val().trim(),
		structure: $("#inp-structure").val().trim(),
		dtsxd: $("#inp-dtsxd").val().trim(),
		clcl: $("#inp-clcl").val().trim(),
		latitude: $("#inp-latitude").val().trim(),
		longtitude: $("#inp-longtitude").val().trim(),
		approved: "0",
	};
	if ($("#inp-street-house").is(":checked")) {
		vDataObj.streetHouse = 1;
	} else {
		vDataObj.streetHouse = 0;
	}
	if ($("#inp-fsbo").is(":checked")) {
		vDataObj.fsbo = 1;
	} else {
		vDataObj.fsbo = 0;
	}

	return vDataObj;
}

//validate data
//input: data obj
//output: data obj
function validateData(paramDataObj) {
	if (paramDataObj.address == "") {
		loadErrorModal("Please enter Address");
		return false;
	}
	if (isNaN(paramDataObj.price)) {
		loadErrorModal("Please enter a Price as a number");
		return false;
	}
	if (isNaN(paramDataObj.priceMin)) {
		loadErrorModal("Please enter a Price Min as a number");
		return false;
	}
	if (isNaN(paramDataObj.wallArea)) {
		loadErrorModal("Please enter Wall Area as a number");
		return false;
	}
	if (isNaN(paramDataObj.bedroom)) {
		loadErrorModal("Please enter Bedroom as a number");
		return false;
	}
	if (isNaN(paramDataObj.priceRent)) {
		loadErrorModal("Please enter Price Rent as a number");
		return false;
	}
	if (isNaN(paramDataObj.returnRate)) {
		loadErrorModal("Please enter Return rate as a number");
		return false;
	}
	if (isNaN(paramDataObj.acreage)) {
		loadErrorModal("Please enter Acreage as a number");
		return false;
	}
	if (isNaN(paramDataObj.totalFloors)) {
		loadErrorModal("Please enter Total Floors as a number");
		return false;
	}
	if (isNaN(paramDataObj.numberFloors)) {
		loadErrorModal("Please enter Number of floors as a number");
		return false;
	}
	if (isNaN(paramDataObj.bath)) {
		loadErrorModal("Please enter Bath as a number");
		return false;
	}
	if (isNaN(paramDataObj.legalDoc)) {
		loadErrorModal("Please enter Legal Doc as a number");
		return false;
	}
	if (isNaN(paramDataObj.widthY)) {
		loadErrorModal("Please enter Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.longX)) {
		loadErrorModal("Please enter Long as a number");
		return false;
	}
	if (isNaN(paramDataObj.viewNum)) {
		loadErrorModal("Please enter View Number as a number");
		return false;
	}
	if (isNaN(paramDataObj.distance2Facade)) {
		loadErrorModal("Please enter Distance to Facade as a number");
		return false;
	}
	if (isNaN(paramDataObj.adjacentFacadeNum)) {
		loadErrorModal("Please enter Adjacent Facade Num as a number");
		return false;
	}
	if (isNaN(paramDataObj.ctxdPrice)) {
		loadErrorModal("Please enter CTXD Price as a number");
		return false;
	}
	if (isNaN(paramDataObj.ctxdValue)) {
		loadErrorModal("Please enter CTXD Value as a number");
		return false;
	}

	if (isNaN(paramDataObj.alleyMinWidth)) {
		loadErrorModal("Please enter Alley Min Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.adjacentAlleyMinWidth)) {
		loadErrorModal("Please enter Adjacent Alley Min Width as a number");
		return false;
	}
	if (isNaN(paramDataObj.dtsxd)) {
		loadErrorModal("Please enter DTSXD as a number");
		return false;
	}

	if (isNaN(paramDataObj.clcl)) {
		loadErrorModal("Please enter CLCL as a number");
		return false;
	}
	if (isNaN(paramDataObj.latitude)) {
		loadErrorModal("Please enter Latitude as a number");
		return false;
	}
	if (isNaN(paramDataObj.longtitude)) {
		loadErrorModal("Please enter Longtitude as a number");
		return false;
	}

	return true;
}

//load error modal
//input: error message
function loadErrorModal(paramErrorMessage) {
	$("#p-error").text(paramErrorMessage);
	$("#modal-error").modal("show");
}
