"use strict";

import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gToken = utils.getAndValidateToken(window.location.href);
var gURL = "http://localhost:8080/realestates";
var gBASE_URL = "http://localhost:8080/";
var gDistrictId;
var gProvinceId;
var gWardId;
var gStreetId;
var gId;
/*** REGION 2 - Elements' events declaration */
// when page load
$(document).ready(function () {
	//load provinces to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "provinces",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-province").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
			$("#sel-province").val(gProvinceId).trigger("change");
		},
	});

	// get the data from home page and load to search
	var vUrlParams = new URLSearchParams(window.location.search);
	gId = JSON.parse(vUrlParams.get("id"));
	callAPIGetData(gId);
});

//when a province is chosen
$("#sel-province").change(function () {
	var vProvinceId = $("#sel-province").val();
	$("#sel-district").val("").trigger("change");
	$("#sel-district :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-district").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load ward
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-ward :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/wards",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-ward").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load street
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-street :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/streets",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-street").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});

// Initiallize ionRangeSlider
$("#inp-price").ionRangeSlider({
	min: 0,
	max: 50000000000,
	from: 0,
	to: 50000000000,
	type: "double",
	step: 1,
	prefix: "VND ",
	prettify: false,
	hasGrid: true,
});

//when btn search is clicked
$("#btn-search").click(function () {
	// get data
	var vDataObj = getData();
	var vUrl = "properties.html?data=" + encodeURIComponent(JSON.stringify(vDataObj));
	window.location.href = vUrl;
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//get data
function getData() {
	var vDataObj = {
		provinceId: $("#sel-province").val(),
		districtId: $("#sel-district").val(),
		wardsId: $("#sel-ward").val(),
		streetId: $("#sel-street").val(),
		type: $("#sel-type").val(),
		request: $("#sel-request").val(),
		bedroom: $("#sel-bedroom").val(),
		priceLow: $("#inp-price").val().split(";")[0],
		priceHigh: $("#inp-price").val().split(";")[1],
		title: $("#inp-title").val().trim(),
	};
	return vDataObj;
}

//call api get data
//input: id of data
function callAPIGetData(paramId) {
	$.ajax({
		type: "GET",
		url: gURL + "/" + paramId,
		success: function (response) {
			loadDataToPage(response);
		},
		error: function (error) {},
	});
}

//load data to page
//input: data obj
function loadDataToPage(paramDataObj) {
	loadDataToSpecificationTable(paramDataObj);
	$("#h-title").text(paramDataObj.title);
	$("#span-address").text(paramDataObj.address);
	$("#img-property").attr("src", gBASE_URL + "uploads/" + paramDataObj.photo);
	callAPIGetProvinceById(paramDataObj.provinceId);
	callAPIGetDistrictById(paramDataObj.districtId);
	callAPIGetStreetById(paramDataObj.streetId);
	callAPIGetWardById(paramDataObj.wardsId);
	callAPIGetProjectById(paramDataObj.projectId);
	callAPIGetCustomerById(paramDataObj.customerId);
	$("#span-price")
		.text((+paramDataObj.price).toLocaleString("en-US") + " VND")
		.css("font-weight", "bold");
	$("#span-acreage")
		.text(paramDataObj.acreage + " m2")
		.css("font-weight", "bold");
	$("#p-description").text(paramDataObj.description);
}

//load data to table
//input: data obj
function loadDataToSpecificationTable(paramDataObj) {
	console.log(paramDataObj);
	var vTableBody = $("#table-specification tbody");
	var vHideArray = ["id", "request", "dateCreate", "createBy", "updateBy", "photo", "viewNum"];
	Object.entries(paramDataObj).forEach(([name, value]) => {
		if (!vHideArray.includes(name)) {
			var vRow = $("<tr>");
			var vCellName = $("<td>");
			var vCellValue = $("<td>");
			switch (name) {
				case "request":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Cần bán");
							break;
						case 1:
							vCellValue.text("Cần mua");
							break;
						case 2:
							vCellValue.text("Cho thuê");
							break;
						case 3:
							vCellValue.text("Cần thuê");
							break;
					}
					break;
				case "type":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Đất");
							break;
						case 1:
							vCellValue.text("Nhà ở");
							break;
						case 2:
							vCellValue.text("Căn hộ chung cư");
							break;
						case 3:
							vCellValue.text("Văn phòng mặt bằng");
							break;
						case 4:
							vCellValue.text("Kinh doanh");
							break;
						case 5:
							vCellValue.text("Phòng trọ");
							break;
						default:
							break;
					}
					break;
				case "apartLoca":
					vCellName.text("Apartment Location");
					vCellValue.text(value);
					break;
				case "apartType":
					vCellName.text("Apartment Type");
					vCellValue.text(value);
					break;
				case "furnitureType":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Đầy đủ");
							break;
						case 1:
							vCellValue.text("Không có");
							break;
						default:
							break;
					}
					break;
				case "distance2Facade":
					vCellName.text("Distance To Facade");
					vCellValue.text(value);
					break;
				case "direction":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Tây Bắc");
							break;
						case 1:
							vCellValue.text("Tây");
							break;
						case 2:
							vCellValue.text("Bắc");
							break;
						case 3:
							vCellValue.text("Đông");
							break;
						case 4:
							vCellValue.text("Nam");
							break;
						case 5:
							vCellValue.text("Tây Nam");
							break;
						case 6:
							vCellValue.text("Đông Bắc");
							break;
						case 7:
							vCellValue.text("Đông Nam");
							break;
						case 8:
							vCellValue.text("Tây Nam, Đông Nam");
							break;
						case 9:
							vCellValue.text("Tây Nam, Tây Bắc");
							break;
						case 10:
							vCellValue.text("Không rõ");
							break;
						case 11:
							vCellValue.text("Đông Bắc, Đông Nam");
							break;
						case 12:
							vCellValue.text("Đông Bắc, Tây Bắc");
							break;
						default:
							break;
					}
					break;
				case "priceTime":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Bán nhanh 24h");
							break;
						case 1:
							vCellValue.text("Bán nhanh 72h");
							break;
						case 2:
							vCellValue.text("Bán nhanh 1 tuần");
							break;
						case 3:
							vCellValue.text("Cần thuê");
							break;
						default:
							break;
					}
					break;
				case "balcony":
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					switch (value) {
						case 0:
							vCellValue.text("Không có");
							break;
						case 1:
							vCellValue.text("Ban công");
							break;
						case 2:
							vCellValue.text("Lô gia");
							break;
						default:
							break;
					}
					break;
				default:
					vCellName.text(
						name
							.replace("Id", "")
							.replace(/([A-Z])/g, " $1")

							.replace(/^./, function (str) {
								return str.toUpperCase();
							})
					);
					vCellValue.text(value);
					break;
			}

			vRow.append(vCellName);
			vRow.append(vCellValue);
			vTableBody.append(vRow);
		}
	});
}

//call api get province by provinceId
//input: provinceId
function callAPIGetProvinceById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "provinces/" + paramId,
		success: function (response) {
			$("#span-province").text(response.name);
			$('td:contains("Province")').next().text(response.name);
		},
		error: function (error) {
			console.log(error);
		},
	});
}

//call api get district by districtId
//input: districtId
function callAPIGetDistrictById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "districts/" + paramId,
		success: function (response) {
			$("#span-district").text(", " + response.name);
			$('td:contains("District")').next().text(response.name);
		},
		error: function (error) {
			console.log(error);
		},
	});
}

//call api get ward by wardId
//input: wardId
function callAPIGetWardById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "wards/" + paramId,
		success: function (response) {
			$("#span-ward").text(", " + response.name);
			$('td:contains("Wards")').next().text(response.name);
		},
		error: function (error) {
			console.log(error);
			$("td:eq(7)", row);
		},
	});
}

//call api get street by streetId
//input: streetId
function callAPIGetStreetById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "streets/" + paramId,
		success: function (response) {
			$("#span-street").text(", " + response.name);
			$('td:contains("Street")').next().text(response.name);
		},
		error: function (error) {
			console.log(error);
		},
	});
}

//call api get customer by customerId
//input: customerId
function callAPIGetCustomerById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "customers/" + paramId,
		success: function (response) {
			$('td:contains("Customer")')
				.next()
				.text(response.contactName + " " + response.mobile);
		},
		error: function (error) {
			console.log(error);
		},
	});
}
//call api get project by projectId
//input: projectId
function callAPIGetProjectById(paramId) {
	$.ajax({
		type: "GET",
		url: gBASE_URL + "projects/" + paramId,
		success: function (response) {
			$('td:contains("Project")').next().text(response.name);
		},
		error: function (error) {
			console.log(error);
		},
	});
}
