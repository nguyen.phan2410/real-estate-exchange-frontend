"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
var gURL_COUNT = "http://localhost:8080/realestates/search/count";
var gURL = "http://localhost:8080/realestates/search";
var gBASE_URL = "http://localhost:8080/";
var gDistrictId;
var gProvinceId;
var gWardId;
var gStreetId;
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
// when page load
$(document).ready(function () {
	//load provinces to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "provinces",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-province").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
			$("#sel-province").val(gProvinceId).trigger("change");
		},
	});

	// get the data from home page and load to search
	var vUrlParams = new URLSearchParams(window.location.search);
	var vDataObj = JSON.parse(vUrlParams.get("data"));
	loadDataToPage(vDataObj);
	callAPIGetNumberOfProperties(vDataObj);
});

//when a province is chosen load district
$("#sel-province").change(function () {
	var vProvinceId = $("#sel-province").val();
	$("#sel-district").val("").trigger("change");
	$("#sel-district :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-district").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gDistrictId) {
					$("#sel-district").val(gDistrictId).trigger("change");
					gDistrictId = null;
				}
			});
		},
	});
});

//when a district is chosen load ward
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-ward").val("").trigger("change");
	$("#sel-ward :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/wards",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-ward").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gWardId) {
					$("#sel-ward").val(gWardId).trigger("change");
					gWardId = null;
				}
			});
		},
	});
});

//when a district is chosen load street
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-street").val("").trigger("change");
	$("#sel-street :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: gBASE_URL + "districts/" + vDistrictId + "/streets",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-street").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
				if (data.id == gStreetId) {
					$("#sel-street").val(gStreetId).trigger("change");
					gStreetId = null;
				}
			});
		},
	});
});

//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});

// Initiallize ionRangeSlider
$("#inp-price").ionRangeSlider({
	min: 0,
	max: 50000000000,
	from: 0,
	to: 50000000000,
	type: "double",
	step: 1,
	prefix: "VND ",
	prettify: false,
	hasGrid: true,
});

//when btn search is clicked
$("#btn-search").click(function () {
	// get data
	var vDataObj = getData();
	// save the new search object into query
	var vCurrentUrl = window.location.protocol + "//" + window.location.host + window.location.pathname;
	var vNewUrl = "properties.html?data=" + encodeURIComponent(JSON.stringify(vDataObj));
	window.history.pushState({ path: vCurrentUrl }, "", vNewUrl);
	// call api get number of properties
	console.log(vDataObj);
	callAPIGetNumberOfProperties(vDataObj);
});
/*** REGION 3 - Event handlers */

/*** REGION 4 - Common funtions */
//get data
//output: data object
function getData() {
	var vDataObj = {
		provinceId: $("#sel-province").val(),
		districtId: $("#sel-district").val(),
		wardsId: $("#sel-ward").val(),
		streetId: $("#sel-street").val(),
		type: $("#sel-type").val(),
		request: $("#sel-request").val(),
		bedroom: $("#sel-bedroom").val(),
		priceLow: $("#inp-price").val().split(";")[0],
		priceHigh: $("#inp-price").val().split(";")[1],
		title: $("#inp-title").val().trim(),
	};
	return vDataObj;
}

//call api to get the number of properties
function callAPIGetNumberOfProperties(paramDataObj) {
	var vItemPerPage = 8;
	$.ajax({
		type: "POST",
		url: gURL_COUNT,
		data: JSON.stringify(paramDataObj),
		contentType: "application/json",
		success: function (response) {
			console.log(response);
			if (response > 0) {
				$("#div-properties").empty();
				$(".pagination").pagination({
					items: response,
					itemsOnPage: vItemPerPage,
					cssStyle: "light-theme",
					onPageClick(pageNumber) {
						console.log(pageNumber);
						callAPIGetProperties(paramDataObj, pageNumber, vItemPerPage);
					},
					onInit() {
						callAPIGetProperties(paramDataObj, 1, vItemPerPage);
					},
				});
			} else {
				$("#div-properties").empty();
				$(".pagination").empty();
				$("#div-properties").append("<h2 class='text-center mt-5' style='font-family: 'lora''>No result</h2>");
			}
		},
		error: function (error) {
			console.log(error);
		},
	});
}

//call api to get properties
//input: search obj, page, size
function callAPIGetProperties(paramDataObj, paramPage, paramSize) {
	$.ajax({
		type: "POST",
		url: gURL + "?page=" + paramPage + "&size=" + paramSize,
		data: JSON.stringify(paramDataObj),
		contentType: "application/json",
		success: function (response) {
			loadPropertiesToPage(response);
		},
		error: function (error) {
			console.log(error);
		},
	});
}

//load properties to page
//input: list of properties
function loadPropertiesToPage(paramProperties) {
	var vDivLoadProperties = $("#div-properties");
	vDivLoadProperties.empty();
	paramProperties.forEach((property) => {
		vDivLoadProperties.append(
			`
            <div class=" col-sm-12 col-md-6 col-lg-3 p-2 d-flex justify-content-center" id="card-` +
				property.id +
				`"  >
                <div class="card" style="width: 95%; height:450px" >
                    <img class="card-img-top" src=" ` +
				gBASE_URL +
				"uploads/" +
				property.photo +
				`" alt="Card image" onError='$(this).attr("src","assets/images/background/no-img.jpg");' height="250px">
                    <div class="card-body d-flex flex-column" >
                        <h4 class="card-title">` +
				property.title +
				`</h4>
                        <p class="card-text"  style=" white-space: nowrap;overflow: hidden; text-overflow: ellipsis;"> 
						<i class="fas fa-dollar-sign"></i> <b>` +
				(+property.price).toLocaleString("en-US") +
				" VND" +
				`</b>
				</br>
				<i class="fas fa-bed"></i> <b>` +
				property.bedroom +
				`</b>
				<i class="fas fa-bath"></i> <b>` +
				property.bath +
				`</b>
				</p>
                        <a href=" ` +
				"properties-detail.html?id=" +
				property.id +
				`" class="btn btn-primary mt-auto">See Property</a>
                    </div>
                </div>
            </div>`
		);
	});
}

function loadDataToPage(paramDataObj) {
	$("#inp-title").val(paramDataObj.title);
	gProvinceId = paramDataObj.provinceId;
	$("#sel-province").val(gDistrictId).trigger("change");
	gDistrictId = paramDataObj.districtId;
	$("#sel-district").val(gDistrictId).trigger("change");
	gWardId = paramDataObj.wardsId;
	$("#sel-ward").val(gWardId).trigger("change");
	gStreetId = paramDataObj.streetId;
	$("#sel-street").val(gStreetId).trigger("change");
	$("#sel-bedroom").val(paramDataObj.bedroom);
	$("#sel-type").val(paramDataObj.type).trigger("change");
	$("#sel-request").val(paramDataObj.request).trigger("change");
	$("#inp-price").data("ionRangeSlider").update({
		from: paramDataObj.priceLow,
		to: paramDataObj.priceHigh,
	});
}
