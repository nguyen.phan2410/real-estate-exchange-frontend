"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gToken = utils.getAndValidateToken(window.location.href);
const gURl_SIGN_UP = "http://localhost:8080/auth/signup";
const gURL_SIGN_IN = "http://localhost:8080/auth/signin";
/*** REGION 2 - Elements' events declaration */
//on sign in btn click
$("#form-sign-in").on("submit", function (e) {
	e.preventDefault();
	// get data
	var vUserObj = {
		username: $("#inp-sign-in-username").val().trim(),
		password: $("#inp-sign-in-password").val().trim(),
	};
	var vValid = validateSignInUser(vUserObj);
	if (vValid) {
		callAPISignIn(vUserObj);
	}
});

//on register btn click
$("#form-register").on("submit", function (e) {
	e.preventDefault();
	// get data
	var vUserObj = {
		username: $("#inp-register-username").val().trim(),
		password: $("#inp-register-password").val().trim(),
		email: $("#inp-register-email").val().trim(),
	};
	var vValid = validateRegisterUser(vUserObj);
	if (vValid) {
		callAPIRegister(vUserObj);
	}
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//call api register
function callAPIRegister(paramUserObj) {
	$.ajax({
		url: gURl_SIGN_UP + "/Customer",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(paramUserObj),
		success: function (response) {
			callAPISignIn(paramUserObj);
		},
		error: function (error) {
			try {
				var errorMessage = error.responseText;
				console.log(errorMessage);
				if (errorMessage.includes("Email")) {
					$("#p-register-email-error").text(errorMessage);
				} else if (errorMessage.includes("Username")) {
					$("#p-register-username-error").text(errorMessage);
				}
			} catch (err) {
				console.log(error);
				toastr.error("Error");
			}
		},
	});
}

//call api sign in
function callAPISignIn(paramUserObj) {
	$.ajax({
		url: gURL_SIGN_IN + "/Customer",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(paramUserObj),
		success: function (response) {
			console.log(response);
			// if remember me is checked then use persistent cookie
			if ($("#inp-sign-in-remember").is(":checked")) {
				utils.setCookie("token", response.accessToken, 1);
			} else {
				utils.setCookie("token", response.accessToken, 0);
			}
			window.location.href = "index.html";
		},
		error: function (error) {
			try {
				var errorMessage = error.responseJSON.message;
				$("#p-sign-in-username-error").text(errorMessage);
				$("#p-sign-in-password-error").text(errorMessage);
			} catch (err) {
				console.log(error);
				toastr.error("Error");
			}
		},
	});
}

//validate user
//input: user obj
//output: true if user is valid, false otherwise
function validateRegisterUser(paramUserObj) {
	var result = true;
	if (paramUserObj.username == "") {
		$("#p-register-username-error").text("Please enter a Username");
		result = false;
	} else {
		$("#p-register-username-error").text("");
	}

	if (paramUserObj.email == "") {
		$("#p-register-email-error").text("Please enter a Email");
		result = false;
	} else if (!validateEmail(paramUserObj.email)) {
		$("#p-register-email-error").text("Please enter a Valid Email");
		result = false;
	} else {
		$("#p-register-email-error").text("");
	}

	if (paramUserObj.password == "") {
		$("#p-register-password-error").text("Please enter a password");
		result = false;
	} else if (paramUserObj.password.length < 6 || paramUserObj.password.length > 40) {
		$("#p-register-password-error").text("Password must be between 6 and 40 characters");
		result = false;
	} else {
		$("#p-register-password-error").text("");
	}

	if ($("#inp-register-confirm-password").val().trim() == "") {
		$("#p-register-confirm-password-error").text("Please confirm your password");
		result = false;
	} else if ($("#inp-register-confirm-password").val().trim() != paramUserObj.password) {
		$("#p-register-confirm-password-error").text("Please enter the same password");
		result = false;
	} else {
		$("#p-register-confirm-password-error").text("");
	}
	return result;
}

//validate email
//input: email
//outpuy: true if email is valid, false otherwise
function validateEmail(paramEmail) {
	return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}

//validate user
//input: user obj
//output: true if user is valid, false otherwise
function validateSignInUser(paramUserObj) {
	var result = true;
	if (paramUserObj.username == "") {
		$("#p-sign-in-username-error").text("Please enter a username");
		result = false;
	} else {
		$("#p-sign-in-username-error").text("");
	}
	if (paramUserObj.password == "") {
		$("#p-sign-in-password-error").text("Please enter a password");
		result = false;
	} else if (paramUserObj.password.length < 6 || paramUserObj.password.length > 40) {
		$("#p-sign-in-password-error").text("Password must be between 6 and 40 characters");
		result = false;
	} else {
		$("#p-sign-in-password-error").text("");
	}
	return result;
}
