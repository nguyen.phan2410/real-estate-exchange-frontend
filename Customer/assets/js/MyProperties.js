/*** REGION 1 - Global variables */
"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/realestates";
const gBASE_URL = "http://localhost:8080/";
var gId = new URLSearchParams(window.location.search).get("id");
var gRealesateId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);

/*** REGION 2 - Elements' events declaration */
$(document).ready(function () {
	//get all customer properties and add them to page
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		url: gBASE_URL + "customers/" + gId + "/realestates", // API
		type: "GET",
		success: function (response) {
			loadPropertiesToPage(response);
		},
		error: function (error) {
			console.log(error);
		},
	});
});

// on btn edit is clicked
$("body").on("click", ".btn-edit", function () {
	var gRealesateId = this.id;
	var vUrl = "EditProperty.html?id=" + gId + "&realestateId=" + gRealesateId;
	window.location.href = vUrl;
});

// on btn delete is clicked
$("body").on("click", ".btn-delete", function () {
	gRealesateId = this.id;
	$("#modal-delete").modal("show");
});

// on btn confirm is clicked
$("#btn-confirm").on("click", function () {
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "DELETE",
		url: gURL + "/" + gRealesateId,
		success: function (response) {
			toastr.success("Delete successfully");
			$("#modal-delete").modal("hide");
			window.location.reload();
		},
		error: function (error) {
			toastr.error("Delete failed");
			console.log(error);
		},
	});
});

//on btn add is clicked
$("#btn-add-property").on("click", function () {
	var vUrl = "AddProperty.html?id=" + gId;
	window.location.href = vUrl;
});

/*** REGION 3 - Event handlers */

/*** REGION 4 - Common funtions */
//load properties to page
//input: all properties of a customer
function loadPropertiesToPage(paramProperties) {
	var vDivPending = $("#div-pending");
	var vDivApproved = $("#div-approved");
	var vDivProperties;
	console.log(paramProperties);
	paramProperties.forEach((property) => {
		if (property.approved == 0) {
			vDivProperties = vDivPending;
		} else {
			vDivProperties = vDivApproved;
		}
		vDivProperties.append(
			`
            <div class=" col-sm-12 col-md-6 col-lg-4 p-2 " id="card-` +
				property.id +
				`"  >
                <div class="card" style="width: 95%;" >
                    <img class="card-img-top" src=" ` +
				gBASE_URL +
				"uploads/" +
				property.photo +
				`" alt="Card image" onError='$(this).attr("src","assets/images/background/no-img.jpg");' height="250px">
                    <div class="card-body d-flex flex-column" >
                        <h4 class="card-title">` +
				property.title +
				`</h4>
                        <p class="card-text"  style=" white-space: nowrap;overflow: hidden; text-overflow: ellipsis;"> 
						<i class="fas fa-dollar-sign"></i> <b>` +
				(+property.price).toLocaleString("en-US") +
				" VND" +
				`</b>
				</br>
				<i class="fas fa-bed"></i> <b>` +
				property.bedroom +
				`</b>
				<i class="fas fa-bath"></i> <b>` +
				property.bath +
				`</b>
				</br>
				</br>
				<i id="` +
				property.id +
				`" class="btn btn-primary w-100 fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp;
				</br>
				</br>
				<i id="` +
				property.id +
				`"  class="btn btn-danger w-100 fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>
            </div>`
		);
	});
}
