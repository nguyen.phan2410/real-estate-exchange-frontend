"use strict";

import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gToken = utils.getAndValidateToken(window.location.href);

/*** REGION 2 - Elements' events declaration */
//Initialize Select2 Elements
$(".select2bs4").select2({
	theme: "bootstrap4",
});
$("#inp-price").ionRangeSlider({
	min: 0,
	max: 50000000000,
	from: 0,
	to: 50000000000,
	type: "double",
	step: 1,
	prefix: "VND ",
	prettify: false,
	hasGrid: true,
});
// when page load
$(document).ready(function () {
	//load provinces to select
	$.ajax({
		type: "GET",
		url: "http://localhost:8080/provinces",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-province").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a province is chosen load districts
$("#sel-province").change(function () {
	var vProvinceId = $("#sel-province").val();
	$("#sel-district").val("").trigger("change");
	$("#sel-district :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: "http://localhost:8080/provinces/" + vProvinceId + "/districts",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-district").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load ward
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-ward :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: "http://localhost:8080/districts/" + vDistrictId + "/wards",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-ward").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when a district is chosen load street
$("#sel-district").change(function () {
	var vDistrictId = $("#sel-district").val();
	$("#sel-street :not(:first-child)").remove();
	// load district to select
	$.ajax({
		type: "GET",
		url: "http://localhost:8080/districts/" + vDistrictId + "/streets",
		success: function (response) {
			$.each(response, function (i, data) {
				$("#sel-street").append(
					$("<option>", {
						value: data.id,
						text: data.name,
					})
				);
			});
		},
	});
});

//when btn search is clicked
$("#btn-search").click(function () {
	// get data
	var vDataObj = getData();
	var vUrl = "properties.html?data=" + encodeURIComponent(JSON.stringify(vDataObj));
	window.location.href = vUrl;
});
/*** REGION 3 - Event handlers */

/*** REGION 4 - Common funtions */
//get data
function getData() {
	var vDataObj = {
		provinceId: $("#sel-province").val(),
		districtId: $("#sel-district").val(),
		wardsId: $("#sel-ward").val(),
		streetId: $("#sel-street").val(),
		type: $("#sel-type").val(),
		request: $("#sel-request").val(),
		bedroom: $("#sel-bedroom").val(),
		priceLow: $("#inp-price").val().split(";")[0],
		priceHigh: $("#inp-price").val().split(";")[1],
		title: $("#inp-title").val().trim(),
	};
	return vDataObj;
}
