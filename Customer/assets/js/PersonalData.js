"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gURL = "http://localhost:8080/customers";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
var gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
$("#btn-save").on("click", function () {
	// get data
	var vDataObj = getData();
	// validate data
	var vValid = validateData(vDataObj);
	if (vValid) {
		// add data
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "PUT",
			url: gURL + "/" + gId,
			data: JSON.stringify(vDataObj),
			contentType: "application/json",
			success: function (response) {
				toastr.success("Update successful");
				$("#p-email-error").text();
				$("#p-username-error").text();
				$("#p-mobile-error").text();
				loadDataToPage(response);
				console.log(response);
				if (response.token != null) {
					utils.setCookie("token", response.token, 1);
					gToken = response.token;
				}
			},
			error: function (error) {
				try {
					var errorMessage = error.responseText;
					console.log(errorMessage);
					if (errorMessage.includes("Email")) {
						$("#p-email-error").text(errorMessage);
					} else if (errorMessage.includes("Username")) {
						$("#p-username-error").text(errorMessage);
					} else if (errorMessage.includes("Mobile")) {
						$("#p-mobile-error").text(errorMessage);
					}
				} catch (err) {
					console.log(error);
					toastr.error("Error");
				}
			},
		});
	}
});

$(document).ready(function () {
	var urlParams = new URLSearchParams(window.location.search);
	gId = urlParams.get("id");
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gURL + "/" + gId,
		success: function (response) {
			loadDataToPage(response);
		},
		error: function (error) {
			loadErrorModal("Load data failed");
			$("#modal-error").on("hidden.bs.modal", function () {
				window.location.href = "Customer.html";
			});
		},
	});
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//load data to page
//input: data obj
function loadDataToPage(paramDataObj) {
	$("#inp-contact-title").val(paramDataObj.contactTitle);
	$("#inp-contact-name").val(paramDataObj.contactName);
	$("#inp-address").val(paramDataObj.address);
	$("#inp-mobile").val(paramDataObj.mobile);
	$("#inp-email").val(paramDataObj.email);
	$("#inp-note").val(paramDataObj.note);
	$("#inp-username").val(paramDataObj.username);
}

// get data
// output: data obj
function getData() {
	var vDataObj = {
		contactTitle: $("#inp-contact-title").val().trim(),
		contactName: $("#inp-contact-name").val().trim(),
		address: $("#inp-address").val().trim(),
		mobile: $("#inp-mobile").val().trim(),
		email: $("#inp-email").val().trim(),
		note: $("#inp-note").val().trim(),
		username: $("#inp-username").val().trim(),
		password: $("#inp-password").val().trim(),
	};
	return vDataObj;
}

//validate data
//input: data obj
//output: data obj
function validateData(paramUserObj) {
	var result = true;
	if (paramUserObj.username == "") {
		$("#p-username-error").text("Please enter a Username");
		result = false;
	} else {
		$("#p-username-error").text("");
	}

	if (paramUserObj.email == "") {
		$("#p-email-error").text("Please enter a Email");
		result = false;
	} else if (!validateEmail(paramUserObj.email)) {
		$("#p-email-error").text("Please enter a Valid Email");
		result = false;
	} else {
		$("#p-email-error").text("");
	}

	if ((paramUserObj.password != "" && paramUserObj.password.length < 6) || paramUserObj.password.length > 40) {
		$("#p-password-error").text("Password must be between 6 and 40 characters");
		result = false;
	} else {
		$("#p-password-error").text("");
	}

	return result;
}

//validate email
//input: email
//outpuy: true if email is valid, false otherwise
function validateEmail(paramEmail) {
	return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}

//load error modal
//input: error message
function loadErrorModal(paramErrorMessage) {
	$("#p-error").text(paramErrorMessage);
	$("#modal-error").modal("show");
}
