var gURL = "http://localhost:8080/auth/me";
//get and validate token
export function getAndValidateToken(paramPage) {
	var vToken = getCookie("token");
	if (vToken == "") {
		removeUserFromPage();
		// if in one of these pages then go back home
		if (paramPage.includes("PersonalData") || paramPage.includes("MyProperties") || paramPage.includes("AddProperty")) {
			window.location.href == "index.html";
		}
	} else {
		callAPIGetUser(vToken, paramPage);
		return vToken;
	}
}

//when logout button is clicked
$("#a-logout").click(function () {
	redirectToHome();
});

//call api get user
//input: token
//If token is validated and user exists, redirect to page according to user role, else redirect to login page
export function callAPIGetUser(paramToken, paramPage) {
	var user;
	$.ajax({
		headers: {
			Authorization: "Bearer " + paramToken,
		},
		type: "GET",
		url: gURL,
		success: function (response) {
			addUserToPage(response);
		},
		error: function (error) {
			console.log(error);
			removeUserFromPage();
		},
	});
}

//add user info to page
function addUserToPage(paramUserObj) {
	$("#li-sign-in").hide();
	$("#li-user").show();
	$("#a-personal-data").attr("href", "PersonalData.html?id=" + paramUserObj.id);
	$("#a-my-properties").attr("href", "MyProperties.html?id=" + paramUserObj.id);
	$("#a-add-property").attr("href", "AddProperty.html?id=" + paramUserObj.id);
}

//remove user from page
function removeUserFromPage() {
	//remove user from page
	$("#li-sign-in").show();
	$("#li-user").hide();
}

// redirect to sign in
export function redirectToHome() {
	// delete the saved token
	setCookie("token", "", 1);
	window.location.href = "index.html";
}

// set cookie
//input: cookie name, cookie value, days to expire
export function setCookie(cname, cvalue, exdays) {
	// don't set persistent cookie if exdays = 0
	if (exdays == 0) {
		document.cookie = cname + "=" + cvalue + ";path=/";
	} else {
		var d = new Date();
		d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
}

//get cookie
//input: cookie name
//output: token
export function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
