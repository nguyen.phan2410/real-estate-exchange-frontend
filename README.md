# Real Estate Frontend Project

This project is a frontend web application for managing real estate listings. It includes a customer-facing website for browsing property listings and an admin dashboard for managing listings and databases.

## Features

### Customer Website

- Browse all property listings
- Filter listings 
- View detailed information about each property
- Create customer listings.

### Admin Dashboard

- Manage properties (create, update, delete)
- Manage all the needed databases
- Different levels of users (Admin, HomeSeller, Default, Anonymous)
