"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/realestates";
const gBASE_URL = "http://localhost:8080/";

var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);

/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	// when page load
	$(document).ready(function () {
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});

		//load customers to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "customers",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-customer").append(
						$("<option>", {
							value: data.id,
							text: data.contactName + ", " + data.mobile,
						})
					);
				});
			},
		});
	});

	//when a province is chosen load districts
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a district is chosen load ward
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-ward").val("").trigger("change");
		$("#sel-ward :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/wards",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-ward").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a district is chosen load street
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-street").val("").trigger("change");
		$("#sel-street :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/streets",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-street").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a district is chosen load project
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-project").val("").trigger("change");
		$("#sel-project :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/projects",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-project").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	//on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		console.log(vDataObj);
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// create form data with vDataObj and File
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append("image", new Blob());
			}

			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Realestate.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			address: $("#inp-address").val().trim(),
			photo: "",
			provinceId: $("#sel-province").val().trim(),
			districtId: $("#sel-district").val().trim(),
			wardsId: $("#sel-ward").val().trim(),
			streetId: $("#sel-street").val().trim(),
			projectId: $("#sel-project").val().trim(),
			title: $("#inp-title").val().trim(),
			type: $("#sel-type").val().trim(),
			request: $("#sel-request").val().trim(),
			customerId: $("#sel-customer").val().trim(),
			price: $("#inp-price").val().trim(),
			priceMin: $("#inp-price-min").val().trim(),
			priceTime: $("#sel-price-time").val().trim(),
			apartCode: $("#inp-apart-code").val().trim(),
			wallArea: $("#inp-wall-area").val().trim(),
			bedroom: $("#inp-bedroom").val().trim(),
			balcony: $("#sel-balcony").val().trim(),
			landscapeView: $("#inp-landscape-view").val().trim(),
			apartLoca: $("#sel-apart-loca").val().trim(),
			apartType: $("#sel-apart-type").val().trim(),
			furnitureType: $("#sel-furniture-type").val().trim(),
			priceRent: $("#inp-price-rent").val().trim(),
			returnRate: $("#inp-return-rate").val().trim(),
			acreage: $("#inp-acreage").val().trim(),
			direction: $("#sel-direction").val().trim(),
			totalFloors: $("#inp-total-floors").val().trim(),
			numberFloors: $("#inp-number-floors").val().trim(),
			bath: $("#inp-bath").val().trim(),
			legalDoc: $("#inp-legal-doc").val().trim(),
			description: $("#inp-description").val().trim(),
			widthY: $("#inp-width-y").val().trim(),
			longX: $("#inp-long-x").val().trim(),
			streetHouse: "",
			fsbo: "",
			viewNum: $("#inp-view-number").val().trim(),
			shape: $("#inp-shape").val().trim(),
			distance2Facade: $("#inp-distance-2-facade").val().trim(),
			adjacentFacadeNum: $("#inp-adjacent-facade-num").val().trim(),
			adjacentRoad: $("#inp-adjacent-road").val().trim(),
			ctxdPrice: $("#inp-ctxd-price").val().trim(),
			ctxdValue: $("#inp-ctxd-value").val().trim(),
			alleyMinWidth: $("#inp-alley-min-width").val().trim(),
			adjacentAlleyMinWidth: $("#inp-adjacent-alley-min-width").val().trim(),
			factor: $("#inp-factor").val().trim(),
			structure: $("#inp-structure").val().trim(),
			dtsxd: $("#inp-dtsxd").val().trim(),
			clcl: $("#inp-clcl").val().trim(),
			latitude: $("#inp-latitude").val().trim(),
			longtitude: $("#inp-longtitude").val().trim(),
			approved: "",
		};
		if ($("#inp-street-house").is(":checked")) {
			vDataObj.streetHouse = 1;
		} else {
			vDataObj.streetHouse = 0;
		}
		if ($("#inp-fsbo").is(":checked")) {
			vDataObj.fsbo = 1;
		} else {
			vDataObj.fsbo = 0;
		}
		if ($("#inp-approved").is(":checked")) {
			vDataObj.approved = 1;
		} else {
			vDataObj.approved = 0;
		}
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.address == "") {
			loadErrorModal("Please enter Address");
			return false;
		}
		if (isNaN(paramDataObj.price)) {
			loadErrorModal("Please enter a Price as a number");
			return false;
		}
		if (isNaN(paramDataObj.priceMin)) {
			loadErrorModal("Please enter a Price Min as a number");
			return false;
		}
		if (isNaN(paramDataObj.wallArea)) {
			loadErrorModal("Please enter Wall Area as a number");
			return false;
		}
		if (isNaN(paramDataObj.bedroom)) {
			loadErrorModal("Please enter Bedroom as a number");
			return false;
		}
		if (isNaN(paramDataObj.priceRent)) {
			loadErrorModal("Please enter Price Rent as a number");
			return false;
		}
		if (isNaN(paramDataObj.returnRate)) {
			loadErrorModal("Please enter Return rate as a number");
			return false;
		}
		if (isNaN(paramDataObj.acreage)) {
			loadErrorModal("Please enter Acreage as a number");
			return false;
		}
		if (isNaN(paramDataObj.totalFloors)) {
			loadErrorModal("Please enter Total Floors as a number");
			return false;
		}
		if (isNaN(paramDataObj.numberFloors)) {
			loadErrorModal("Please enter Number of floors as a number");
			return false;
		}
		if (isNaN(paramDataObj.bath)) {
			loadErrorModal("Please enter Bath as a number");
			return false;
		}
		if (isNaN(paramDataObj.legalDoc)) {
			loadErrorModal("Please enter Legal Doc as a number");
			return false;
		}
		if (isNaN(paramDataObj.widthY)) {
			loadErrorModal("Please enter Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.longX)) {
			loadErrorModal("Please enter Long as a number");
			return false;
		}
		if (isNaN(paramDataObj.viewNum)) {
			loadErrorModal("Please enter View Number as a number");
			return false;
		}
		if (isNaN(paramDataObj.distance2Facade)) {
			loadErrorModal("Please enter Distance to Facade as a number");
			return false;
		}
		if (isNaN(paramDataObj.adjacentFacadeNum)) {
			loadErrorModal("Please enter Adjacent Facade Num as a number");
			return false;
		}
		if (isNaN(paramDataObj.ctxdPrice)) {
			loadErrorModal("Please enter CTXD Price as a number");
			return false;
		}
		if (isNaN(paramDataObj.ctxdValue)) {
			loadErrorModal("Please enter CTXD Value as a number");
			return false;
		}

		if (isNaN(paramDataObj.alleyMinWidth)) {
			loadErrorModal("Please enter Alley Min Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.adjacentAlleyMinWidth)) {
			loadErrorModal("Please enter Adjacent Alley Min Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.dtsxd)) {
			loadErrorModal("Please enter DTSXD as a number");
			return false;
		}

		if (isNaN(paramDataObj.clcl)) {
			loadErrorModal("Please enter CLCL as a number");
			return false;
		}
		if (isNaN(paramDataObj.latitude)) {
			loadErrorModal("Please enter Latitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.longtitude)) {
			loadErrorModal("Please enter Longtitude as a number");
			return false;
		}

		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	var gDistrictId;
	var gProvinceId;
	var gWardId;
	var gStreetId;
	var gProjectId;
	var gCustomerId;
	var gPhoto;
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	// on image can't load picture
	$("#img").on("error", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});
	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	// on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data obj and file to form data
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append(
					"image",
					new Blob(),
					$("#img")
						.attr("src")
						.substring($("#img").attr("src").lastIndexOf("/") + 1)
				);
			}
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Realestate.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		// load data to page
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Realestate.html";
				});
			},
		});
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-province").val(gProvinceId).trigger("change");
			},
		});

		//load customers to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "customers",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-customer").append(
						$("<option>", {
							value: data.id,
							text: data.contactName + ", " + data.mobile,
						})
					);
				});
				$("#sel-customer").val(gCustomerId).trigger("change");
			},
		});
	});

	//when a province is chosen load district
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gDistrictId) {
						$("#sel-district").val(gDistrictId).trigger("change");
						gDistrictId = null;
					}
				});
			},
		});
	});

	//when a district is chosen load ward
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-ward").val("").trigger("change");
		$("#sel-ward :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/wards",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-ward").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gWardId) {
						$("#sel-ward").val(gWardId).trigger("change");
						gWardId = null;
					}
				});
			},
		});
	});

	//when a district is chosen load street
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-street").val("").trigger("change");
		$("#sel-street :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/streets",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-street").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gStreetId) {
						$("#sel-street").val(gStreetId).trigger("change");
						gStreetId = null;
					}
				});
			},
		});
	});

	//when a district is chosen load project
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-project").val("").trigger("change");
		$("#sel-project :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/projects",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-project").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);

					if (data.id == gProjectId) {
						$("#sel-project").val(gProjectId).trigger("change");
						gProjectId = null;
					}
				});
			},
		});
	});

	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#inp-address").val(paramDataObj.address);
		$("#img").attr("src", gBASE_URL + "uploads/" + paramDataObj.photo);
		gPhoto = paramDataObj.photo;
		$("#show-photo").show();
		gProvinceId = paramDataObj.provinceId;
		$("#sel-province").val(gDistrictId).trigger("change");
		gDistrictId = paramDataObj.districtId;
		$("#sel-district").val(gDistrictId).trigger("change");
		gWardId = paramDataObj.wardsId;
		$("#sel-ward").val(gWardId).trigger("change");
		gStreetId = paramDataObj.streetId;
		$("#sel-street").val(gStreetId).trigger("change");
		gProjectId = paramDataObj.projectId;
		$("#sel-project").val(gProjectId).trigger("change");
		$("#inp-title").val(paramDataObj.title);
		$("#sel-type").val(paramDataObj.type).trigger("change");
		$("#sel-request").val(paramDataObj.request).trigger("change");
		gCustomerId = paramDataObj.customerId;
		$("#sel-customer").val(gCustomerId).trigger("change");
		$("#inp-price").val(paramDataObj.price);
		$("#inp-price-min").val(paramDataObj.priceMin);
		$("#sel-price-time").val(paramDataObj.priceTime).trigger("change");
		$("#inp-apart-code").val(paramDataObj.apartCode);
		$("#inp-wall-area").val(paramDataObj.wallArea);
		$("#inp-bedroom").val(paramDataObj.bedroom);
		$("#sel-balcony").val(paramDataObj.balcony).trigger("change");
		$("#inp-landscape-view").val(paramDataObj.landscapeView);
		$("#sel-apart-loca").val(paramDataObj.apartLoca).trigger("change");
		$("#sel-apart-type").val(paramDataObj.apartType).trigger("change");
		$("#sel-furniture-type").val(paramDataObj.furnitureType).trigger("change");
		$("#inp-price-rent").val(paramDataObj.priceRent);
		$("#inp-return-rate").val(paramDataObj.returnRate);
		$("#inp-acreage").val(paramDataObj.acreage);
		$("#sel-direction").val(paramDataObj.direction).trigger("change");
		$("#inp-total-floors").val(paramDataObj.totalFloors);
		$("#inp-number-floors").val(paramDataObj.numberFloors);
		$("#inp-bath").val(paramDataObj.bath);
		$("#inp-legal-doc").val(paramDataObj.legalDoc);
		$("#inp-description").val(paramDataObj.description);
		$("#inp-width-y").val(paramDataObj.widthY);
		$("#inp-long-x").val(paramDataObj.longX);
		if (paramDataObj.streetHouse == 1) {
			$("#inp-street-house").prop("checked", true);
		} else {
			$("#inp-street-house").prop("checked", false);
		}
		if (paramDataObj.fsbo == 1) {
			$("#inp-fsbo").prop("checked", true);
		} else {
			$("#inp-fsbo").prop("checked", false);
		}
		$("#inp-view-number").val(paramDataObj.viewNum);
		$("#inp-shape").val(paramDataObj.shape);
		$("#inp-distance-2-facade").val(paramDataObj.distance2Facade);
		$("#inp-adjacent-facade-num").val(paramDataObj.adjacentFacadeNum);
		$("#inp-adjacent-road").val(paramDataObj.adjacentRoad);
		$("#inp-ctxd-price").val(paramDataObj.ctxdPrice);
		$("#inp-ctxd-value").val(paramDataObj.ctxdValue);
		$("#inp-alley-min-width").val(paramDataObj.alleyMinWidth);
		$("#inp-adjacent-alley-min-width").val(paramDataObj.adjacentAlleyMinWidth);
		$("#inp-factor").val(paramDataObj.factor);
		$("#inp-structure").val(paramDataObj.structure);
		$("#inp-dtsxd").val(paramDataObj.dtsxd);
		$("#inp-clcl").val(paramDataObj.clcl);
		$("#inp-latitude").val(paramDataObj.latitude);
		$("#inp-longtitude").val(paramDataObj.longtitude);
		if (paramDataObj.approved == 1) {
			$("#inp-approved").prop("checked", true);
		} else {
			$("#inp-approved").prop("checked", false);
		}
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			address: $("#inp-address").val().trim(),
			photo: gPhoto,
			provinceId: $("#sel-province").val(),
			districtId: $("#sel-district").val(),
			wardsId: $("#sel-ward").val(),
			streetId: $("#sel-street").val(),
			projectId: $("#sel-project").val(),
			title: $("#inp-title").val().trim(),
			type: $("#sel-type").val(),
			request: $("#sel-request").val(),
			customerId: $("#sel-customer").val(),
			price: $("#inp-price").val().trim(),
			priceMin: $("#inp-price-min").val().trim(),
			priceTime: $("#sel-price-time").val(),
			apartCode: $("#inp-apart-code").val().trim(),
			wallArea: $("#inp-wall-area").val().trim(),
			bedroom: $("#inp-bedroom").val().trim(),
			balcony: $("#sel-balcony").val(),
			landscapeView: $("#inp-landscape-view").val().trim(),
			apartLoca: $("#sel-apart-loca").val(),
			apartType: $("#sel-apart-type").val(),
			furnitureType: $("#sel-furniture-type").val(),
			priceRent: $("#inp-price-rent").val().trim(),
			returnRate: $("#inp-return-rate").val().trim(),
			acreage: $("#inp-acreage").val().trim(),
			direction: $("#sel-direction").val(),
			totalFloors: $("#inp-total-floors").val().trim(),
			numberFloors: $("#inp-number-floors").val().trim(),
			bath: $("#inp-bath").val().trim(),
			legalDoc: $("#inp-legal-doc").val().trim(),
			description: $("#inp-description").val().trim(),
			widthY: $("#inp-width-y").val().trim(),
			longX: $("#inp-long-x").val().trim(),
			streetHouse: "",
			fsbo: "",
			viewNum: $("#inp-view-number").val().trim(),
			shape: $("#inp-shape").val().trim(),
			distance2Facade: $("#inp-distance-2-facade").val().trim(),
			adjacentFacadeNum: $("#inp-adjacent-facade-num").val().trim(),
			adjacentRoad: $("#inp-adjacent-road").val().trim(),
			ctxdPrice: $("#inp-ctxd-price").val().trim(),
			ctxdValue: $("#inp-ctxd-value").val().trim(),
			alleyMinWidth: $("#inp-alley-min-width").val().trim(),
			adjacentAlleyMinWidth: $("#inp-adjacent-alley-min-width").val().trim(),
			factor: $("#inp-factor").val().trim(),
			structure: $("#inp-structure").val().trim(),
			dtsxd: $("#inp-dtsxd").val().trim(),
			clcl: $("#inp-clcl").val().trim(),
			latitude: $("#inp-latitude").val().trim(),
			longtitude: $("#inp-longtitude").val().trim(),
			approved: "",
		};
		if ($("#inp-street-house").is(":checked")) {
			vDataObj.streetHouse = 1;
		} else {
			vDataObj.streetHouse = 0;
		}
		if ($("#inp-fsbo").is(":checked")) {
			vDataObj.fsbo = 1;
		} else {
			vDataObj.fsbo = 0;
		}
		if ($("#inp-approved").is(":checked")) {
			vDataObj.approved = 1;
		} else {
			vDataObj.approved = 0;
		}
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.address == "") {
			loadErrorModal("Please enter Address");
			return false;
		}
		if (isNaN(paramDataObj.price)) {
			loadErrorModal("Please enter a Price as a number");
			return false;
		}
		if (isNaN(paramDataObj.priceMin)) {
			loadErrorModal("Please enter a Price Min as a number");
			return false;
		}
		if (isNaN(paramDataObj.wallArea)) {
			loadErrorModal("Please enter Wall Area as a number");
			return false;
		}

		if (isNaN(paramDataObj.bedroom)) {
			loadErrorModal("Please enter Bedroom as a number");
			return false;
		}
		if (isNaN(paramDataObj.priceRent)) {
			loadErrorModal("Please enter Price Rent as a number");
			return false;
		}
		if (isNaN(paramDataObj.returnRate)) {
			loadErrorModal("Please enter Return rate as a number");
			return false;
		}
		if (isNaN(paramDataObj.acreage)) {
			loadErrorModal("Please enter Acreage as a number");
			return false;
		}
		if (isNaN(paramDataObj.totalFloors)) {
			loadErrorModal("Please enter Total Floors as a number");
			return false;
		}
		if (isNaN(paramDataObj.numberFloors)) {
			loadErrorModal("Please enter Number of floors as a number");
			return false;
		}
		if (isNaN(paramDataObj.bath)) {
			loadErrorModal("Please enter Bath as a number");
			return false;
		}
		if (isNaN(paramDataObj.legalDoc)) {
			loadErrorModal("Please enter Legal Doc as a number");
			return false;
		}
		if (isNaN(paramDataObj.widthY)) {
			loadErrorModal("Please enter Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.longX)) {
			loadErrorModal("Please enter Long as a number");
			return false;
		}
		if (isNaN(paramDataObj.viewNum)) {
			loadErrorModal("Please enter View Number as a number");
			return false;
		}
		if (isNaN(paramDataObj.distance2Facade)) {
			loadErrorModal("Please enter Distance to Facade as a number");
			return false;
		}
		if (isNaN(paramDataObj.adjacentFacadeNum)) {
			loadErrorModal("Please enter Adjacent Facade Num as a number");
			return false;
		}
		if (isNaN(paramDataObj.ctxdPrice)) {
			loadErrorModal("Please enter CTXD Price as a number");
			return false;
		}
		if (isNaN(paramDataObj.ctxdValue)) {
			loadErrorModal("Please enter CTXD Value as a number");
			return false;
		}

		if (isNaN(paramDataObj.alleyMinWidth)) {
			loadErrorModal("Please enter Alley Min Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.adjacentAlleyMinWidth)) {
			loadErrorModal("Please enter Adjacent Alley Min Width as a number");
			return false;
		}
		if (isNaN(paramDataObj.dtsxd)) {
			loadErrorModal("Please enter DTSXD as a number");
			return false;
		}

		if (isNaN(paramDataObj.clcl)) {
			loadErrorModal("Please enter CLCL as a number");
			return false;
		}
		if (isNaN(paramDataObj.latitude)) {
			loadErrorModal("Please enter Latitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.longtitude)) {
			loadErrorModal("Please enter Longtitude as a number");
			return false;
		}

		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "approved", "provinceId", "districtId", "wardsId", "streetId", "type", "request", "customerId", "price", "dateCreate", "acreage", "totalFloors", "numberFloors", "widthY", "longX", "photo", "priceTime"];
	var colname;
	var gTable = $("#table-data").DataTable({
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		searching: false,
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: gURL + "/dataTable/search", // API
			type: "POST",
			data: function (data) {
				data.provinceId = $("#sel-province").val().trim();
				data.districtId = $("#sel-district").val().trim();
				data.projectId = $("#sel-project").val().trim();
				data.title = $("#inp-title").val().trim();
				data.customerId = $("#sel-customer").val().trim();
			},
		},
		columns: [
			{ data: gNAME[0] },
			{ data: gNAME[1] },
			{ data: gNAME[2] },
			{ data: gNAME[3] },
			{ data: gNAME[4] },
			{ data: gNAME[5] },
			{ data: gNAME[6] },
			{ data: gNAME[7] },
			{ data: gNAME[8] },
			{ data: gNAME[9] },
			{ data: gNAME[10] },
			{ data: gNAME[11] },
			{ data: gNAME[12] },
			{ data: gNAME[13] },
			{ data: gNAME[14] },
			{ data: gNAME[15] },
			{ data: gNAME[16] },
			{ data: gNAME[17] },
			{ data: gNAME[18] },
		],
		order: [[2, "asc"]],
		scrollX: true,
		rowCallback: function (row, data) {
			// add provinces
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "provinces/" + data.provinceId,
				success: function (response) {
					$("td:eq(3)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add district
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "districts/" + data.districtId,
				success: function (response) {
					$("td:eq(4)", row).html(response.prefix + " " + response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add ward
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "wards/" + data.wardsId,
				success: function (response) {
					$("td:eq(5)", row).html(response.prefix + " " + response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add street
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "streets/" + data.streetId,
				success: function (response) {
					$("td:eq(6)", row).html(response.prefix + " " + response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			//add type
			switch (data.type) {
				case 0:
					$("td:eq(7)", row).html("Đất");
					break;
				case 1:
					$("td:eq(7)", row).html("Nhà ở");
					break;
				case 2:
					$("td:eq(7)", row).html("Căn hộ/Chung cư");
					break;
				case 3:
					$("td:eq(7)", row).html("Văn phòng, Mặt bằng");
					break;
				case 4:
					$("td:eq(7)", row).html("Kinh doanh");
					break;
				case 5:
					$("td:eq(7)", row).html("Phòng trọ");
			}

			// add request
			switch (data.request) {
				case 0:
					$("td:eq(8)", row).html("Cần bán");
					break;
				case 1:
					$("td:eq(8)", row).html("Cần mua");
					break;
				case 2:
					$("td:eq(8)", row).html("Cho thuê");
					break;
				case 3:
					$("td:eq(8)", row).html("Cần thuê");
					break;
			}
			// add customer
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "customers/" + data.customerId,
				success: function (response) {
					$("td:eq(9)", row).html(response.contactName);
				},
				error: function (error) {
					console.log(error);
				},
			});

			//add selling time
			switch (data.priceTime) {
				case 0:
					$("td:eq(18)", row).html("Bán nhanh 24h");
					break;
				case 1:
					$("td:eq(18)", row).html("Bán nhanh 72h");
					break;
				case 2:
					$("td:eq(18)", row).html("Bán nhanh 1 tuần");
					break;
				case 3:
					$("td:eq(18)", row).html("Cần thuê");
					break;
			}

			//load photo
			$("td:eq(17)", row).html("<img src=" + gBASE_URL + "uploads/" + data.photo + " height='80px' width='80px' onError='this.remove();'>");

			//load approved
			if (data.approved == 1) {
				$("td:eq(2)", row).html("YES");
			} else {
				$("td:eq(2)", row).html("NO");
			}
		},
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */
	// when page load
	$(document).ready(function () {
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});

		//load customers to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "customers",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-customer").append(
						$("<option>", {
							value: data.id,
							text: data.contactName + ", " + data.mobile,
						})
					);
				});
			},
		});
	});

	//when a province is chosen load districts
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a district is chosen load project
	$("#sel-district").change(function () {
		var vDistrictId = $("#sel-district").val();
		$("#sel-project").val("").trigger("change");
		$("#sel-project :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "districts/" + vDistrictId + "/projects",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-project").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when search btn is clicked
	$("#btn-search").click(function () {
		$("#modal-search").modal("show");
	});

	//when find btn is clicked
	$("#btn-find").click(function () {
		$("#table-data").DataTable().ajax.reload();
		$("#modal-search").modal("hide");
	});

	//when reset btn is clicked
	$("#btn-reset").click(function () {
		$("#form-search").trigger("reset");
		$("#sel-province").val("").change();
		$("#sel-district").val("").change();
		$("#sel-customer").val("").change();
		$("#sel-project").val("").change();
		$("#table-data").DataTable().ajax.reload();
		$("#modal-search").modal("hide");
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Realestate-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Realestate-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
