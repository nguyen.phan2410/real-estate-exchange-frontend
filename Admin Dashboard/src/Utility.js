"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/utilitys";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	// on btn save clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		console.log(vDataObj);
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// create form data with vDataObj and File
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append("image", new Blob());
			}
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Utility.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			description: $("#inp-description").val().trim(),
			photo: "",
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.name == "") {
			loadErrorModal("Please enter Name");
			return false;
		}

		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	var gPhoto;
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	// on image can't load picture
	$("#img").on("error", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data obj and file to form data
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append(
					"image",
					new Blob(),
					$("#img")
						.attr("src")
						.substring($("#img").attr("src").lastIndexOf("/") + 1)
				);
			}

			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Utility.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");

		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Utility.html";
				});
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		console.log(paramDataObj);
		$("#inp-name").val(paramDataObj.name);
		$("#inp-description").val(paramDataObj.description);

		$("#img").attr("src", gBASE_URL + "uploads/" + paramDataObj.photo);
		gPhoto = paramDataObj.photo;
		$("#show-photo").show();
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			description: $("#inp-description").val().trim(),
			photo: gPhoto,
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.name == "") {
			loadErrorModal("Please enter Name");
			return false;
		}

		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "name", "description", "photo"];
	var colname;
	var gTable = $("#table-data").DataTable({
		searching: false,
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: "http://localhost:8080/utilitys/dataTable", // API
		},
		columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }],
		order: [[1, "asc"]],
		scrollX: true,
		rowCallback: function (row, data) {
			//load photo
			//load photo
			$("td:eq(4)", row).html("<img src=" + gBASE_URL + "uploads/" + data.photo + " height='80px' width='80px' onError='this.remove();'>");
		},
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Utility-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Utility-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
