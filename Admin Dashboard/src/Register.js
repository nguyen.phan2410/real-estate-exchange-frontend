"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gToken = utils.getCookie("token");
const gURl_SIGN_UP = "http://localhost:8080/auth/signup";
const gURL_SIGN_IN = "http://localhost:8080/auth/signin";
/*** REGION 2 - Elements' events declaration */

//on register btn click
$("#form-register").on("submit", function (e) {
	e.preventDefault();
	// get data
	var vUserObj = {
		username: $("#inp-username").val().trim(),
		password: $("#inp-password").val().trim(),
		firstName: $("#inp-first-name").val().trim(),
		lastName: $("#inp-last-name").val().trim(),
		email: $("#inp-email").val().trim(),
	};
	var vValid = validateUser(vUserObj);
	if (validateUser(vUserObj)) {
		callAPIRegister(vUserObj);
	}
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */

//call api register
function callAPIRegister(paramUserObj) {
	$.ajax({
		url: gURl_SIGN_UP + "/Employee",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(paramUserObj),
		success: function (response) {
			callAPISignIn(paramUserObj);
		},
		error: function (error) {
			try {
				var errorMessage = error.responseText;
				console.log(errorMessage);
				if (errorMessage.includes("Email")) {
					$("#p-email-error").text(errorMessage);
				} else if (errorMessage.includes("Username")) {
					$("#p-username-error").text(errorMessage);
				}
			} catch (err) {
				console.log(error);
				toastr.error("Error");
			}
		},
	});
}

//call api sign in
function callAPISignIn(paramUserObj) {
	$.ajax({
		url: gURL_SIGN_IN + "/Employee",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(paramUserObj),
		success: function (response) {
			utils.setCookie("token", response.accessToken, 1);
			window.location.href = "Home.html";
		},
		error: function (error) {
			console.log(error);
			toastr.error("Error");
		},
	});
}

//validate user
//input: user obj
//output: true if user is valid, false otherwise
function validateUser(paramUserObj) {
	var result = true;
	if (paramUserObj.username == "") {
		$("#p-username-error").text("Please enter a Username");
		result = false;
	} else {
		$("#p-username-error").text("");
	}

	if (paramUserObj.firstName == "") {
		$("#p-first-name-error").text("Please enter a First Name");
		result = false;
	} else {
		$("#p-first-name-error").text("");
	}

	if (paramUserObj.lastName == "") {
		$("#p-last-name-error").text("Please enter a Last Name");
		result = false;
	} else {
		$("#p-last-name-error").text("");
	}

	if (paramUserObj.email == "") {
		$("#p-email-error").text("Please enter a Email");
		result = false;
	} else if (!validateEmail(paramUserObj.email)) {
		$("#p-email-error").text("Please enter a Valid Email");
		result = false;
	} else {
		$("#p-email-error").text("");
	}

	if (paramUserObj.password == "") {
		$("#p-password-error").text("Please enter a password");
		result = false;
	} else if (paramUserObj.password.length < 6 || paramUserObj.password.length > 40) {
		$("#p-password-error").text("Password must be between 6 and 40 characters");
		result = false;
	} else {
		$("#p-password-error").text("");
	}

	if ($("#inp-confirm-password").val().trim() == "") {
		$("#p-confirm-password-error").text("Please confirm your password");
		result = false;
	} else if ($("#inp-confirm-password").val().trim() != paramUserObj.password) {
		$("#p-confirm-password-error").text("Please enter the same password");
		result = false;
	} else {
		$("#p-confirm-password-error").text("");
	}
	return result;
}

//validate email
//input: email
//outpuy: true if email is valid, false otherwise
function validateEmail(paramEmail) {
	return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}
