"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/customers";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Customer.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					toastr.error(error.responseText);
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			contactTitle: $("#inp-contact-title").val().trim(),
			contactName: $("#inp-contact-name").val().trim(),
			address: $("#inp-address").val().trim(),
			mobile: $("#inp-mobile").val().trim(),
			email: $("#inp-email").val().trim(),
			note: $("#inp-note").val().trim(),
			username: $("#inp-username").val().trim(),
			password: $("#inp-password").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.username == "") {
			loadErrorModal("Please enter username");
			return false;
		}
		if (paramDataObj.password == "") {
			loadErrorModal("Please enter password");
			return false;
		}
		if (paramDataObj.password.length < 6 || paramDataObj.password.length > 40) {
			loadErrorModal("Please enter password longer than 6 characters and shorter than 40 characters");
			return false;
		}
		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}

		if (!validateEmail(paramDataObj.email)) {
			loadErrorModal("Please enter valid Email");
			return false;
		}
		return true;
	}

	//validate email
	//input: email
	//outpuy: true if email is valid, false otherwise
	function validateEmail(paramEmail) {
		return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	}
	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Customer.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					toastr.error(error.responseText);

					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Customer.html";
				});
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#inp-contact-title").val(paramDataObj.contactTitle);
		$("#inp-contact-name").val(paramDataObj.contactName);
		$("#inp-address").val(paramDataObj.address);
		$("#inp-mobile").val(paramDataObj.mobile);
		$("#inp-email").val(paramDataObj.email);
		$("#inp-note").val(paramDataObj.note);
		$("#inp-username").val(paramDataObj.username);
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			contactTitle: $("#inp-contact-title").val().trim(),
			contactName: $("#inp-contact-name").val().trim(),
			address: $("#inp-address").val().trim(),
			mobile: $("#inp-mobile").val().trim(),
			email: $("#inp-email").val().trim(),
			note: $("#inp-note").val().trim(),
			username: $("#inp-username").val().trim(),
			password: $("#inp-password").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.username == "") {
			loadErrorModal("Please enter username");
			return false;
		}
		if (paramDataObj.password != "" && (paramDataObj.password.length < 6 || paramDataObj.password.length > 40)) {
			loadErrorModal("Please enter password longer than 6 characters and shorter than 40 characters");
			return false;
		}
		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}

		if (!validateEmail(paramDataObj.email)) {
			loadErrorModal("Please enter valid Email");
			return false;
		}
		return true;
	}

	//validate email
	//input: email
	//outpuy: true if email is valid, false otherwise
	function validateEmail(paramEmail) {
		return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "contactTitle", "contactName", "address", "mobile", "email", "note", "username", "password", "createBy", "updateBy", "createDate", "updateDate"];
	var colname;
	var gTable = $("#table-data").DataTable({
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		searching: false,
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: gURL + "/dataTable/search", // API
			type: "POST",
			data: function (data) {
				data.contactName = $("#inp-contact-name").val().trim();
				data.email = $("#inp-email").val().trim();
				data.mobile = $("#inp-mobile").val().trim();
			},
		},
		columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }, { data: gNAME[6] }, { data: gNAME[7] }, { data: gNAME[8] }, { data: gNAME[9] }, { data: gNAME[10] }, { data: gNAME[11] }, { data: gNAME[12] }, { data: gNAME[13] }],
		order: [[1, "asc"]],
		scrollX: true,
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Customer-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Customer-add.html";
		window.location.href = vUrl;
	});

	//when search btn is clicked
	$("#btn-search").click(function () {
		$("#modal-search").modal("show");
	});

	//when find btn is clicked
	$("#btn-find").click(function () {
		$("#table-data").DataTable().ajax.reload();
		$("#modal-search").modal("hide");
	});

	//when reset btn is clicked
	$("#btn-reset").click(function () {
		$("#form-search").trigger("reset");
		$("#table-data").DataTable().ajax.reload();
		$("#modal-search").modal("hide");
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
