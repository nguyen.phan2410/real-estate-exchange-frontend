var gURL = "http://localhost:8080/auth/me";
//get and validate token
export function getAndValidateToken(paramPage) {
	var vToken = getCookie("token");
	if (vToken == "") {
		if (!paramPage.includes("SignIn")) {
			window.location.href = "SignIn.html";
		}
	} else {
		// hide all the options in the side bar till further processing
		$("ul.nav-sidebar li").hide();
		callAPIGetUser(vToken, paramPage);
		return vToken;
	}
}

//when logout button is clicked
$("#a-logout").click(function () {
	redirectToSignIn();
});

//call api get user
//input: token
//If token is validated and user exists, redirect to page according to user role, else redirect to login page
export function callAPIGetUser(paramToken, paramPage) {
	var user;
	$.ajax({
		headers: {
			Authorization: "Bearer " + paramToken,
		},
		type: "GET",
		url: gURL,
		success: function (response) {
			console.log(response.userLevel.name);
			addUserToPage(response);
			redirectToPage(response.userLevel.name, paramPage);
		},
		error: function (error) {
			console.log(error);
			redirectToSignIn();
		},
	});
}

//add user info to page
function addUserToPage(paramUserObj) {
	$("#a-personal-data").attr("href", "PersonalData.html?id=" + paramUserObj.id);
	$("#a-username").text(paramUserObj.username);
}

// redirect to sign in
export function redirectToSignIn() {
	// delete the saved token
	setCookie("token", "", 1);
	window.location.href = "SignIn.html";
}

//redirect to page based on account
//input: user role
export function redirectToPage(paramRole, paramPage) {
	switch (paramRole) {
		case "ROLE_ANONYMOUS":
			if (!paramPage.includes("Home") && !paramPage.includes("PersonalData")) {
				window.location.href = "Home.html";
			}

			break;
		case "ROLE_ADMINISTRATOR":
			// change the sidebar to admin mode
			$("ul.nav-sidebar li").show();
			if (paramPage.includes("SignIn") || (paramPage.includes("Register") && !paramPage.includes("PersonalData"))) {
				window.location.href = "AddressMap.html";
			}
			break;
		case "ROLE_HOMESELLER":
			// change the sidebar to homeseller mode
			$("ul.nav-sidebar li:eq(15)").show();
			if (!paramPage.includes("Realestate") && !paramPage.includes("PersonalData")) {
				window.location.href = "Realestate.html";
			}
			break;
		case "ROLE_DEFAULT":
			if (!paramPage.includes("Home") && !paramPage.includes("PersonalData")) {
				window.location.href = "Home.html";
			}
			break;
	}
}

// set cookie
//input: cookie name, cookie value, days to expire
export function setCookie(cname, cvalue, exdays) {
	// don't set persistent cookie if exdays = 0
	if (exdays == 0) {
		document.cookie = cname + "=" + cvalue + ";path=/";
	} else {
		var d = new Date();
		d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
		var expires = "expires=" + d.toUTCString();
		document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	}
}

//get cookie
//input: cookie name
//output: token
export function getCookie(cname) {
	var name = cname + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var ca = decodedCookie.split(";");
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == " ") {
			c = c.substring(1);
		}
		if (c.indexOf(name) == 0) {
			return c.substring(name.length, c.length);
		}
	}
	return "";
}
