"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gToken = utils.getAndValidateToken(window.location.href);
const gURL = "http://localhost:8080/auth/signin";
/*** REGION 2 - Elements' events declaration */
//on sign in btn click
$("#form-sign-in").on("submit", function (e) {
	e.preventDefault();
	// $(this)[0].reset();
	// get data
	var vUserObj = {
		username: $("#inp-username").val().trim(),
		password: $("#inp-password").val().trim(),
	};
	var vValid = validateUser(vUserObj);
	if (validateUser(vUserObj)) {
		callAPISignIn(vUserObj);
	}
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//call api sign in
function callAPISignIn(paramUserObj) {
	$.ajax({
		url: gURL + "/Employee",
		type: "POST",
		contentType: "application/json; charset=utf-8",
		data: JSON.stringify(paramUserObj),
		success: function (response) {
			console.log(response);
			// if remember me is checked then use persistent cookie
			if ($("#inp-remember").is(":checked")) {
				utils.setCookie("token", response.accessToken, 1);
			} else {
				utils.setCookie("token", response.accessToken, 0);
			}
			redirectToPage(response.roles[0]);
		},
		error: function (error) {
			try {
				var errorMessage = error.responseJSON.message;
				$("#p-username-error").text(errorMessage);
				$("#p-password-error").text(errorMessage);
			} catch (err) {
				console.log(error);
				toastr.error("Error");
			}
		},
	});
}

//redirect to page based on account
//input: user role
function redirectToPage(paramRole) {
	switch (paramRole) {
		case "ROLE_ANONYMOUS":
			window.location.href = "Home.html";
			break;
		case "ROLE_ADMINISTRATOR":
			window.location.href = "AddressMap.html";
			break;
		case "ROLE_HOMESELLER":
			window.location.href = "Realestate.html";
			break;
		case "ROLE_DEFAULT":
			window.location.href = "Home.html";
			break;
	}
}

//validate user
//input: user obj
//output: true if user is valid, false otherwise
function validateUser(paramUserObj) {
	var result = true;
	if (paramUserObj.username == "") {
		$("#p-username-error").text("Please enter a username");
		result = false;
	} else {
		$("#p-username-error").text("");
	}
	if (paramUserObj.password == "") {
		$("#p-password-error").text("Please enter a password");
		result = false;
	} else if (paramUserObj.password.length < 6 || paramUserObj.password.length > 40) {
		$("#p-password-error").text("Password must be between 6 and 40 characters");
		result = false;
	} else {
		$("#p-password-error").text("");
	}
	return result;
}
