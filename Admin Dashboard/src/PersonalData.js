"use strict";
import * as utils from "./Utils.js";
/*** REGION 1 - Global variables */
const gURL = "http://localhost:8080/employees";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
var gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
$("#btn-save").on("click", function () {
	// get data
	var vDataObj = getData();
	// validate data
	var vValid = validateData(vDataObj);
	if (vValid) {
		// add data
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "PUT",
			url: gURL + "/" + gId,
			data: JSON.stringify(vDataObj),
			contentType: "application/json",
			success: function (response) {
				if (response.token != null) {
					utils.setCookie("token", response.token, 1);
					gToken = response.token;
				}
				window.location.href = "Employee.html";
			},
			error: function (error) {
				toastr.error("Add failed");
				console.log(error);
			},
		});
	}
});

$(document).ready(function () {
	var urlParams = new URLSearchParams(window.location.search);
	gId = urlParams.get("id");
	$.ajax({
		headers: {
			Authorization: "Bearer " + gToken,
		},
		type: "GET",
		url: gURL + "/" + gId,
		success: function (response) {
			loadDataToPage(response);
		},
		error: function (error) {
			loadErrorModal("Load data failed");
			$("#modal-error").on("hidden.bs.modal", function () {
				window.location.href = "Employee.html";
			});
		},
	});
});
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
//load data to page
//input: data obj
function loadDataToPage(paramDataObj) {
	$("#inp-last-name").val(paramDataObj.lastName);
	$("#inp-first-name").val(paramDataObj.firstName);
	$("#inp-title").val(paramDataObj.title);
	$("#inp-title-of-courtesy").val(paramDataObj.titleOfCourtesy);
	$("#inp-birth-date").val(paramDataObj.birthDate);
	$("#inp-hire-date").val(paramDataObj.hireDate);
	$("#inp-address").val(paramDataObj.address);
	$("#inp-city").val(paramDataObj.city);
	$("#inp-region").val(paramDataObj.region);
	$("#inp-country").val(paramDataObj.country);
	$("#inp-home-phone").val(paramDataObj.homePhone);
	$("#inp-postal-code").val(paramDataObj.postalCode);
	$("#inp-extension").val(paramDataObj.extension);
	$("#inp-photo").val(paramDataObj.photo);
	$("#inp-note").val(paramDataObj.note);
	$("#inp-reports-to").val(paramDataObj.reportsTo);
	$("#inp-username").val(paramDataObj.username);
	$("#inp-email").val(paramDataObj.email);
	if (paramDataObj.activated == "Y") {
		$("#inp-activated").prop("checked", true);
	} else {
		$("#inp-activated").prop("checked", false);
	}
	$("#inp-note").val(paramDataObj.note);
	$("#sel-user-level").val(paramDataObj.userLevel.id);
}

// get data
// output: data obj
function getData() {
	var vDataObj = {
		lastName: $("#inp-last-name").val().trim(),
		firstName: $("#inp-first-name").val().trim(),
		title: $("#inp-title").val().trim(),
		titleOfCourtesy: $("#inp-title-of-courtesy").val().trim(),
		birthDate: $("#inp-birth-date").val().trim(),
		hireDate: $("#inp-hire-date").val().trim(),
		address: $("#inp-address").val().trim(),
		city: $("#inp-city").val().trim(),
		region: $("#inp-region").val().trim(),
		country: $("#inp-country").val().trim(),
		homePhone: $("#inp-home-phone").val().trim(),
		postalCode: $("#inp-postal-code").val().trim(),
		extension: $("#inp-extension").val().trim(),
		photo: $("#inp-photo").val().trim(),
		note: $("#inp-note").val().trim(),
		reportsTo: $("#inp-reports-to").val().trim(),
		username: $("#inp-username").val().trim(),
		password: $("#inp-password").val().trim(),
		email: $("#inp-email").val().trim(),
		activated: "",
		profile: $("#inp-note").val().trim(),
		userLevelId: $("#sel-user-level").val().trim(),
	};

	if ($("#inp-activated").is(":checked")) {
		vDataObj.activated = "Y";
	} else {
		vDataObj.activated = "N";
	}

	return vDataObj;
}

//validate data
//input: data obj
//output: data obj
function validateData(paramDataObj) {
	if (paramDataObj.lastName == "") {
		loadErrorModal("Please enter Last Name");
		return false;
	}

	if (paramDataObj.firstName == "") {
		loadErrorModal("Please enter First Name");
		return false;
	}

	if (paramDataObj.username == "") {
		loadErrorModal("Please enter Username");
		return false;
	}

	if (paramDataObj.password != "" && (paramDataObj.password.length < 6 || paramDataObj.password.length > 40)) {
		loadErrorModal("Please enter password longer than 6 characters and shorter than 40 characters");
		return false;
	}

	if (paramDataObj.email == "") {
		loadErrorModal("Please enter Email");
		return false;
	}
	if (paramDataObj.email == "") {
		loadErrorModal("Please enter Email");
		return false;
	}

	if (!validateEmail(paramDataObj.email)) {
		loadErrorModal("Please enter valid Email");
		return false;
	}
	if (isNaN(paramDataObj.reportsTo)) {
		loadErrorModal("Please enter Reports To as a number");
		return false;
	}

	return true;
}

//validate email
//input: email
//outpuy: true if email is valid, false otherwise
function validateEmail(paramEmail) {
	return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
}

//load error modal
//input: error message
function loadErrorModal(paramErrorMessage) {
	$("#p-error").text(paramErrorMessage);
	$("#modal-error").modal("show");
}
