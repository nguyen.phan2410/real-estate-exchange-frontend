"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/projects";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	// when page load
	$(document).ready(function () {
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
		//load investors to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "investors",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-investor").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
		//load construction contractors to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "constructionContractors",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-construction-contractor").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
		//load design units to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "designUnits",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-design-unit").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
		//load utilities to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "utilitys",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-utility").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
		//load region links to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "regionLinks",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-region-link").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a province is chosen load district
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gDistrictId) {
						$("#sel-district").val(gDistrictId).trigger("change");
						gDistrictId = null;
					}
				});
			},
		});
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	//on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		console.log(vDataObj);
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// create form data with vDataObj and File
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append("image", new Blob());
			}
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Project.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			provinceId: $("#sel-province").val().trim(),
			districtId: $("#sel-district").val().trim(),
			latitude: $("#inp-latitude").val().trim(),
			longtitude: $("#inp-longtitude").val().trim(),
			address: $("#inp-address").val().trim(),
			slogan: $("#inp-slogan").val().trim(),
			description: $("#inp-description").val().trim(),
			acreage: $("#inp-acreage").val().trim(),
			constructArea: $("#inp-construct-area").val().trim(),
			numBlock: $("#inp-num-block").val().trim(),
			numFloors: $("#inp-num-floor").val().trim(),
			numApartment: $("#inp-num-apartment").val().trim(),
			apartmentArea: $("#inp-apartment-area").val(),
			investor: $("#sel-investor").val().trim(),
			constructionContractor: $("#sel-construction-contractor").val().trim(),
			designUnit: $("#sel-design-unit").val().trim(),
			utilities: $("#sel-utility").val().toString(),
			regionLink: $("#sel-region-link").val().toString(),
			photo: "",
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.provinceId == "") {
			loadErrorModal("Please select a province");
			return false;
		}
		if (paramDataObj.districtId == "") {
			loadErrorModal("Please select a district");
			return false;
		}
		if (isNaN(paramDataObj.longtitude)) {
			loadErrorModal("Please enter a Longtitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.latitude)) {
			loadErrorModal("Please enter a Latitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.acreage)) {
			loadErrorModal("Please enter Acreage as a number");
			return false;
		}
		if (isNaN(paramDataObj.constructArea)) {
			loadErrorModal("Please enter Construct Area as a number");
			return false;
		}
		if (isNaN(paramDataObj.numBlock)) {
			loadErrorModal("Please enter Number of block as a number");
			return false;
		}
		if (paramDataObj.numApartment == "") {
			loadErrorModal("Please enter Number of apartments");
			return false;
		}
		if (isNaN(paramDataObj.numApartment)) {
			loadErrorModal("Please enter Number of apartments as a number");
			return false;
		}
		if (paramDataObj.investor == "") {
			loadErrorModal("Please select a Investor");
			return false;
		}
		if (paramDataObj.utilities == "") {
			loadErrorModal("Please select a Utility");
			return false;
		}
		if (paramDataObj.regionLink == "") {
			loadErrorModal("Please select a Region Link");
			return false;
		}
		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	var gDistrictId;
	var gProvinceId;
	var gInvestorId;
	var gConstructionContractorId;
	var gDesignUnitId;
	var gUtilityId;
	var gRegionLinkId;
	var gPhoto;
	/*** REGION 2 - Elements' events declaration */
	// load selected photo
	$(":file").change(function () {
		var src = URL.createObjectURL(this.files[0]);
		$("#img").attr("src", src);
		$("#show-photo").show();
	});

	//on btn delete photo
	$("#btn-delete").on("click", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});

	// on image can't load picture
	$("#img").on("error", function () {
		$("#img").attr("src", "");
		$("#show-photo").hide();
		$("#inp-photo").val("");
	});
	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	// on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data obj and file to form data
			var vFormData = new FormData();
			Object.entries(vDataObj).forEach(([key, value]) => {
				vFormData.append(key, value);
			});
			if ($("#inp-photo")[0].files.length > 0) {
				vFormData.append("image", $("#inp-photo")[0].files[0]);
			} else {
				vFormData.append(
					"image",
					new Blob(),
					$("#img")
						.attr("src")
						.substring($("#img").attr("src").lastIndexOf("/") + 1)
				);
			}
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: vFormData,
				contentType: false,
				processData: false,
				success: function (response) {
					window.location.href = "Project.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		// load data to page
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Project.html";
				});
			},
		});
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-province").val(gProvinceId).trigger("change");
			},
		});
		//load investors to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "investors",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-investor").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-investor").val(gInvestorId).trigger("change");
			},
		});
		//load construction contractors to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "constructionContractors",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-construction-contractor").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-construction-contractor").val(gConstructionContractorId).trigger("change");
			},
		});
		//load design units to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "designUnits",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-design-unit").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-design-unit").val(gDesignUnitId).trigger("change");
			},
		});
		//load utilities to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "utilitys",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-utility").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-utility").val(gUtilityId.split(",")).trigger("change");
			},
		});
		//load region links to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "regionLinks",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-region-link").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-region-link").val(gRegionLinkId.split(",")).trigger("change");
			},
		});
	});
	//when a province is chosen load district
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gDistrictId) {
						$("#sel-district").val(gDistrictId).trigger("change");
						gDistrictId = null;
					}
				});
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#inp-name").val(paramDataObj.name);
		gProvinceId = paramDataObj.provinceId;
		$("#sel-province").val(gProvinceId).trigger("change");
		gDistrictId = paramDataObj.districtId;
		$("#sel-district").val(gDistrictId).trigger("change");
		$("#inp-latitude").val(paramDataObj.latitude);
		$("#inp-longtitude").val(paramDataObj.longtitude);
		$("#inp-address").val(paramDataObj.address);
		$("#inp-slogan").val(paramDataObj.slogan);
		$("#inp-description").val(paramDataObj.description);
		$("#inp-acreage").val(paramDataObj.acreage);
		$("#inp-construct-area").val(paramDataObj.constructArea);
		$("#inp-num-block").val(paramDataObj.numBlock);
		$("#inp-num-floor").val(paramDataObj.numFloors);
		$("#inp-num-apartment").val(paramDataObj.numApartment);
		$("#inp-apartment-area").val(paramDataObj.apartmentArea);
		gInvestorId = paramDataObj.investor;
		$("#sel-investor").val(gInvestorId).trigger("change");
		gConstructionContractorId = paramDataObj.constructionContractor;
		$("#sel-construction-contractor").val(gConstructionContractorId).trigger("change");
		gDesignUnitId = paramDataObj.designUnit;
		$("#sel-design-unit").val(gDesignUnitId).trigger("change");
		gUtilityId = paramDataObj.utilities;
		$("#sel-utility").val(gUtilityId.split(",")).trigger("change");
		gRegionLinkId = paramDataObj.regionLink;
		$("#sel-region-link").val(gRegionLinkId.split(",")).trigger("change");
		$("#img").attr("src", gBASE_URL + "uploads/" + paramDataObj.photo);
		gPhoto = paramDataObj.photo;
		$("#show-photo").show();
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			provinceId: $("#sel-province").val().trim(),
			districtId: $("#sel-district").val().trim(),
			latitude: $("#inp-latitude").val().trim(),
			longtitude: $("#inp-longtitude").val().trim(),
			address: $("#inp-address").val().trim(),
			slogan: $("#inp-slogan").val().trim(),
			description: $("#inp-description").val().trim(),
			acreage: $("#inp-acreage").val().trim(),
			constructArea: $("#inp-construct-area").val().trim(),
			numBlock: $("#inp-num-block").val().trim(),
			numFloors: $("#inp-num-floor").val().trim(),
			numApartment: $("#inp-num-apartment").val().trim(),
			apartmentArea: $("#inp-apartment-area").val(),
			investor: $("#sel-investor").val().trim(),
			constructionContractor: $("#sel-construction-contractor").val().trim(),
			designUnit: $("#sel-design-unit").val().trim(),
			utilities: $("#sel-utility").val().toString(),
			regionLink: $("#sel-region-link").val().toString(),
			photo: gPhoto,
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.provinceId == "") {
			loadErrorModal("Please select a province");
			return false;
		}
		if (paramDataObj.districtId == "") {
			loadErrorModal("Please select a district");
			return false;
		}
		if (isNaN(paramDataObj.longtitude)) {
			loadErrorModal("Please enter a Longtitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.latitude)) {
			loadErrorModal("Please enter a Latitude as a number");
			return false;
		}
		if (isNaN(paramDataObj.acreage)) {
			loadErrorModal("Please enter Acreage as a number");
			return false;
		}
		if (isNaN(paramDataObj.constructArea)) {
			loadErrorModal("Please enter Construct Area as a number");
			return false;
		}
		if (isNaN(paramDataObj.numBlock)) {
			loadErrorModal("Please enter Number of block as a number");
			return false;
		}
		if (paramDataObj.numApartment == "") {
			loadErrorModal("Please enter Number of apartments");
			return false;
		}
		if (isNaN(paramDataObj.numApartment)) {
			loadErrorModal("Please enter Number of apartments as a number");
			return false;
		}
		if (paramDataObj.investor == "") {
			loadErrorModal("Please select a Investor");
			return false;
		}
		if (paramDataObj.utilities == "") {
			loadErrorModal("Please select a Utility");
			return false;
		}
		if (paramDataObj.regionLink == "") {
			loadErrorModal("Please select a Region Link");
			return false;
		}
		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "name", "provinceId", "districtId", "latitude", "longtitude", "acreage", "constructArea", "numBlock", "numApartment", "investor", "constructionContractor", "designUnit"];
	var colname;
	var gTable = $("#table-data").DataTable({
		searching: false,
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: gURL + "/dataTable", // API
		},
		columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }, { data: gNAME[6] }, { data: gNAME[7] }, { data: gNAME[8] }, { data: gNAME[9] }, { data: gNAME[10] }, { data: gNAME[11] }, { data: gNAME[12] }, { data: gNAME[13] }],
		order: [[1, "asc"]],
		scrollX: true,
		rowCallback: function (row, data) {
			// add provinces
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "provinces/" + data.provinceId,
				success: function (response) {
					$("td:eq(3)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add district
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "districts/" + data.districtId,
				success: function (response) {
					$("td:eq(4)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add investor
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "investors/" + data.investor,
				success: function (response) {
					$("td:eq(11)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add construction contractor
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "constructionContractors/" + data.constructionContractor,
				success: function (response) {
					$("td:eq(12)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add design unit
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "designUnits/" + data.designUnit,
				success: function (response) {
					$("td:eq(13)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});
		},
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Project-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Project-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
