"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/employees";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Employee.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			lastName: $("#inp-last-name").val().trim(),
			firstName: $("#inp-first-name").val().trim(),
			title: $("#inp-title").val().trim(),
			titleOfCourtesy: $("#inp-title-of-courtesy").val().trim(),
			birthDate: $("#inp-birth-date").val().trim(),
			hireDate: $("#inp-hire-date").val().trim(),
			address: $("#inp-address").val().trim(),
			city: $("#inp-city").val().trim(),
			region: $("#inp-region").val().trim(),
			country: $("#inp-country").val().trim(),
			homePhone: $("#inp-home-phone").val().trim(),
			postalCode: $("#inp-postal-code").val().trim(),
			extension: $("#inp-extension").val().trim(),
			photo: $("#inp-photo").val().trim(),
			note: $("#inp-note").val().trim(),
			reportsTo: $("#inp-reports-to").val().trim(),
			username: $("#inp-username").val().trim(),
			password: $("#inp-password").val().trim(),
			email: $("#inp-email").val().trim(),
			activated: "",
			profile: $("#inp-note").val().trim(),
			userLevelId: $("#sel-user-level").val().trim(),
		};

		if ($("#inp-activated").is(":checked")) {
			vDataObj.activated = "Y";
		} else {
			vDataObj.activated = "N";
		}

		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.lastName == "") {
			loadErrorModal("Please enter Last Name");
			return false;
		}

		if (paramDataObj.firstName == "") {
			loadErrorModal("Please enter First Name");
			return false;
		}

		if (paramDataObj.username == "") {
			loadErrorModal("Please enter Username");
			return false;
		}

		if (paramDataObj.password == "") {
			loadErrorModal("Please enter Password");
			return false;
		}

		if (paramDataObj.password != "" && (paramDataObj.password.length < 6 || paramDataObj.password.length > 40)) {
			loadErrorModal("Please enter password longer than 6 characters and shorter than 40 characters");
			return false;
		}

		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}
		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}

		if (!validateEmail(paramDataObj.email)) {
			loadErrorModal("Please enter valid Email");
			return false;
		}
		if (isNaN(paramDataObj.reportsTo)) {
			loadErrorModal("Please enter Reports To as a number");
			return false;
		}

		return true;
	}

	//validate email
	//input: email
	//outpuy: true if email is valid, false otherwise
	function validateEmail(paramEmail) {
		return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Employee.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Employee.html";
				});
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#inp-last-name").val(paramDataObj.lastName);
		$("#inp-first-name").val(paramDataObj.firstName);
		$("#inp-title").val(paramDataObj.title);
		$("#inp-title-of-courtesy").val(paramDataObj.titleOfCourtesy);
		$("#inp-birth-date").val(paramDataObj.birthDate);
		$("#inp-hire-date").val(paramDataObj.hireDate);
		$("#inp-address").val(paramDataObj.address);
		$("#inp-city").val(paramDataObj.city);
		$("#inp-region").val(paramDataObj.region);
		$("#inp-country").val(paramDataObj.country);
		$("#inp-home-phone").val(paramDataObj.homePhone);
		$("#inp-postal-code").val(paramDataObj.postalCode);
		$("#inp-extension").val(paramDataObj.extension);
		$("#inp-photo").val(paramDataObj.photo);
		$("#inp-note").val(paramDataObj.note);
		$("#inp-reports-to").val(paramDataObj.reportsTo);
		$("#inp-username").val(paramDataObj.username);
		$("#inp-email").val(paramDataObj.email);
		if (paramDataObj.activated == "Y") {
			$("#inp-activated").prop("checked", true);
		} else {
			$("#inp-activated").prop("checked", false);
		}
		$("#inp-note").val(paramDataObj.note);
		$("#sel-user-level").val(paramDataObj.userLevel.id);
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			lastName: $("#inp-last-name").val().trim(),
			firstName: $("#inp-first-name").val().trim(),
			title: $("#inp-title").val().trim(),
			titleOfCourtesy: $("#inp-title-of-courtesy").val().trim(),
			birthDate: $("#inp-birth-date").val().trim(),
			hireDate: $("#inp-hire-date").val().trim(),
			address: $("#inp-address").val().trim(),
			city: $("#inp-city").val().trim(),
			region: $("#inp-region").val().trim(),
			country: $("#inp-country").val().trim(),
			homePhone: $("#inp-home-phone").val().trim(),
			postalCode: $("#inp-postal-code").val().trim(),
			extension: $("#inp-extension").val().trim(),
			photo: $("#inp-photo").val().trim(),
			note: $("#inp-note").val().trim(),
			reportsTo: $("#inp-reports-to").val().trim(),
			username: $("#inp-username").val().trim(),
			password: $("#inp-password").val().trim(),
			email: $("#inp-email").val().trim(),
			activated: "",
			profile: $("#inp-note").val().trim(),
			userLevelId: $("#sel-user-level").val().trim(),
		};

		if ($("#inp-activated").is(":checked")) {
			vDataObj.activated = "Y";
		} else {
			vDataObj.activated = "N";
		}

		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.lastName == "") {
			loadErrorModal("Please enter Last Name");
			return false;
		}

		if (paramDataObj.firstName == "") {
			loadErrorModal("Please enter First Name");
			return false;
		}

		if (paramDataObj.username == "") {
			loadErrorModal("Please enter Username");
			return false;
		}

		if (paramDataObj.password != "" && (paramDataObj.password.length < 6 || paramDataObj.password.length > 40)) {
			loadErrorModal("Please enter password longer than 6 characters and shorter than 40 characters");
			return false;
		}

		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}
		if (paramDataObj.email == "") {
			loadErrorModal("Please enter Email");
			return false;
		}

		if (!validateEmail(paramDataObj.email)) {
			loadErrorModal("Please enter valid Email");
			return false;
		}
		if (isNaN(paramDataObj.reportsTo)) {
			loadErrorModal("Please enter Reports To as a number");
			return false;
		}

		return true;
	}

	//validate email
	//input: email
	//outpuy: true if email is valid, false otherwise
	function validateEmail(paramEmail) {
		return paramEmail.toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|.(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "lastName", "firstName", "title", "titleOfCourtesy", "birthDate", "hireDate", "address", "city", "region", "postalCode", "country", "homePhone", "extension", "photo", "reportsTo", "username", "password", "email", "activated", "userLevel"];
	var colname;
	var gTable = $("#table-data").DataTable({
		searching: false,
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: "http://localhost:8080/employees" + "/dataTable", // API
		},
		columns: [
			{ data: gNAME[0] },
			{ data: gNAME[1] },
			{ data: gNAME[2] },
			{ data: gNAME[3] },
			{ data: gNAME[4] },
			{ data: gNAME[5] },
			{ data: gNAME[6] },
			{ data: gNAME[7] },
			{ data: gNAME[8] },
			{ data: gNAME[9] },
			{ data: gNAME[10] },
			{ data: gNAME[11] },
			{ data: gNAME[12] },
			{ data: gNAME[13] },
			{ data: gNAME[14] },
			{ data: gNAME[15] },
			{ data: gNAME[16] },
			{ data: gNAME[17] },
			{ data: gNAME[18] },
			{ data: gNAME[19] },
			{ data: gNAME[20] },
			{ data: gNAME[21] },
		],
		order: [[1, "asc"]],
		scrollX: true,
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "20%",
			},
			{
				targets: -1,
				render: function (data, type, row) {
					var vUserLevel = row.userLevel.name;
					return vUserLevel;
				},
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Employee-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Employee-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
