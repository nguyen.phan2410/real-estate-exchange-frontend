"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/streets";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	// when page load
	$(document).ready(function () {
		//load provinces to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
			},
		});
	});

	//when a province is chosen load district
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gDistrictId) {
						$("#sel-district").val(gDistrictId).trigger("change");
						gDistrictId = null;
					}
				});
			},
		});
	});

	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});

	//on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();

		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Street.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			prefix: $("#sel-prefix").val().trim(),
			provinceId: $("#sel-province").val().trim(),
			districtId: $("#sel-district").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.provinceId == "") {
			loadErrorModal("Please select a province");
			return false;
		}
		if (paramDataObj.districtId == "") {
			loadErrorModal("Please select a district");
			return false;
		}
		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	var gDistrictId;
	var gProvinceId;
	/*** REGION 2 - Elements' events declaration */
	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	// on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Street.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		// load data to page
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Street.html";
				});
			},
		});
		// load provinces
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-province").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
				});
				$("#sel-province").val(gProvinceId).trigger("change");
			},
		});
	});
	//when a province is chosen load district
	$("#sel-province").change(function () {
		var vProvinceId = $("#sel-province").val();
		$("#sel-district").val("").trigger("change");
		$("#sel-district :not(:first-child)").remove();
		// load district to select
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "provinces/" + vProvinceId + "/districts",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-district").append(
						$("<option>", {
							value: data.id,
							text: data.name,
						})
					);
					if (data.id == gDistrictId) {
						$("#sel-district").val(gDistrictId).trigger("change");
						gDistrictId = null;
					}
				});
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#inp-name").val(paramDataObj.name);
		$("#sel-prefix").val(paramDataObj.prefix).trigger("change");
		gProvinceId = paramDataObj.provinceId;
		$("#sel-province").val(gDistrictId).trigger("change");
		gDistrictId = paramDataObj.districtId;
		$("#sel-district").val(gDistrictId).trigger("change");
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			name: $("#inp-name").val().trim(),
			prefix: $("#sel-prefix").val().trim(),
			provinceId: $("#sel-province").val().trim(),
			districtId: $("#sel-district").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.provinceId == "") {
			loadErrorModal("Please select a province");
			return false;
		}
		if (paramDataObj.districtId == "") {
			loadErrorModal("Please select a district");
			return false;
		}
		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "name", "prefix", "provinceId", "districtId"];
	var colname;
	var gTable = $("#table-data").DataTable({
		searching: false,
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: gURL + "/dataTable", // API
		},
		columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }],
		order: [[1, "asc"]],
		scrollX: true,
		rowCallback: function (row, data) {
			// add street
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "provinces/" + data.provinceId,
				success: function (response) {
					$("td:eq(4)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});

			// add district
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "GET",
				url: gBASE_URL + "districts/" + data.districtId,
				success: function (response) {
					$("td:eq(5)", row).html(response.name);
				},
				error: function (error) {
					console.log(error);
				},
			});
		},
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Street-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Street-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
