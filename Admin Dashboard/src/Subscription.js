"use strict";
import * as utils from "./Utils.js";
const gURL = "http://localhost:8080/subscriptions";
const gBASE_URL = "http://localhost:8080/";
var gId;
// get and validate and redirect to appropriate pages
const gToken = utils.getAndValidateToken(window.location.href);
/*** REGION 2 - Elements' events declaration */
/*** REGION 3 - Event handlers */
/*** REGION 4 - Common funtions */
if (window.location.href.includes("-add.html")) {
	// Code for adding page
	/*** REGION 1 - Global variables */
	/*** REGION 2 - Elements' events declaration */
	// when page load
	$(document).ready(function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "employees",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-user").append(
						$("<option>", {
							value: data.id,
							text: data.firstName + " " + data.lastName,
						})
					);
				});
			},
		});
	});
	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	//on btn save sis clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "POST",
				url: gURL,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Subscription.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			user: $("#sel-user").val().trim(),
			endpoint: $("#inp-endpoint").val().trim(),
			publickey: $("#inp-publickey").val().trim(),
			authenticationtoken: $("#inp-authenticationtoken").val().trim(),
			contentencoding: $("#inp-contentencoding").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.endpoint == "") {
			loadErrorModal("Please enter Endpoint");
			return false;
		}

		if (paramDataObj.publickey == "") {
			loadErrorModal("Please enter Publickey");
			return false;
		}

		if (paramDataObj.authenticationtoken == "") {
			loadErrorModal("Please enter Authenticationtoken");
			return false;
		}

		if (paramDataObj.contentencoding == "") {
			loadErrorModal("Please enter Contentencoding");
			return false;
		}

		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else if (window.location.href.includes("-edit.html")) {
	// Code for edit page
	/*** REGION 1 - Global variables */
	var gUser;
	/*** REGION 2 - Elements' events declaration */
	//Initialize Select2 Elements
	$(".select2bs4").select2({
		theme: "bootstrap4",
	});
	//on btn save is clicked
	$("#btn-save").on("click", function () {
		// get data
		var vDataObj = getData();
		// validate data
		var vValid = validateData(vDataObj);
		if (vValid) {
			// add data
			$.ajax({
				headers: {
					Authorization: "Bearer " + gToken,
				},
				type: "PUT",
				url: gURL + "/" + gId,
				data: JSON.stringify(vDataObj),
				contentType: "application/json",
				success: function (response) {
					window.location.href = "Subscription.html";
				},
				error: function (error) {
					toastr.error("Add failed");
					console.log(error);
				},
			});
		}
	});

	$(document).ready(function () {
		var urlParams = new URLSearchParams(window.location.search);
		gId = urlParams.get("id");
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gURL + "/" + gId,
			success: function (response) {
				loadDataToPage(response);
			},
			error: function (error) {
				loadErrorModal("Load data failed");
				$("#modal-error").on("hidden.bs.modal", function () {
					window.location.href = "Subscription.html";
				});
			},
		});
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "GET",
			url: gBASE_URL + "employees",
			success: function (response) {
				$.each(response, function (i, data) {
					$("#sel-user").append(
						$("<option>", {
							value: data.id,
							text: data.firstName + " " + data.lastName,
						})
					);
				});
				$("#sel-user").val(gUser).trigger("change");
			},
		});
	});
	/*** REGION 3 - Event handlers */
	/*** REGION 4 - Common funtions */
	//load data to page
	//input: data obj
	function loadDataToPage(paramDataObj) {
		$("#sel-user").val(paramDataObj.user).trigger("change");
		gUser = paramDataObj.user;
		$("#inp-endpoint").val(paramDataObj.endpoint);
		$("#inp-publickey").val(paramDataObj.publickey);
		$("#inp-authenticationtoken").val(paramDataObj.authenticationtoken);
		$("#inp-contentencoding").val(paramDataObj.contentencoding);
	}

	// get data
	// output: data obj
	function getData() {
		var vDataObj = {
			user: $("#sel-user").val().trim(),
			endpoint: $("#inp-endpoint").val().trim(),
			publickey: $("#inp-publickey").val().trim(),
			authenticationtoken: $("#inp-authenticationtoken").val().trim(),
			contentencoding: $("#inp-contentencoding").val().trim(),
		};
		return vDataObj;
	}

	//validate data
	//input: data obj
	//output: data obj
	function validateData(paramDataObj) {
		if (paramDataObj.endpoint == "") {
			loadErrorModal("Please enter Endpoint");
			return false;
		}

		if (paramDataObj.publickey == "") {
			loadErrorModal("Please enter Publickey");
			return false;
		}

		if (paramDataObj.authenticationtoken == "") {
			loadErrorModal("Please enter Authenticationtoken");
			return false;
		}

		if (paramDataObj.contentencoding == "") {
			loadErrorModal("Please enter Contentencoding");
			return false;
		}
		return true;
	}

	//load error modal
	//input: error message
	function loadErrorModal(paramErrorMessage) {
		$("#p-error").text(paramErrorMessage);
		$("#modal-error").modal("show");
	}
} else {
	// Code for main page
	/*** REGION 1 - Global variables */
	const gNAME = ["action", "id", "user", "endpoint", "publickey", "authenticationtoken", "contentencoding"];
	var colname;
	var gTable = $("#table-data").DataTable({
		searching: false,
		processing: true, // shows loading image while fetching data
		serverSide: true, // activates server side pagination
		ajax: {
			headers: {
				Authorization: "Bearer " + gToken,
			},
			url: gURL + "/dataTable", // API
		},
		columns: [{ data: gNAME[0] }, { data: gNAME[1] }, { data: gNAME[2] }, { data: gNAME[3] }, { data: gNAME[4] }, { data: gNAME[5] }, { data: gNAME[6] }],
		order: [[1, "asc"]],
		scrollX: true,
		columnDefs: [
			{
				targets: 0,
				defaultContent: `<i class="fas fa-edit btn-edit" data-toggle="tooltip"  title="Edit" style="cursor: pointer;"></i> &nbsp; &nbsp;&nbsp;
					        <i class="fas fa-trash btn-delete" data-toggle="tooltip" title="Delete" style="cursor: pointer;" ></i>`,
				orderable: false,
				width: "7%",
			},
		],
	});

	/*** REGION 2 - Elements' events declaration */

	// on btn edit is clicked
	$("#table-data").on("click", ".btn-edit", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		var vUrl = "Subscription-edit.html?id=" + gId;
		window.location.href = vUrl;
	});

	// on btn delete is clicked
	$("#table-data").on("click", ".btn-delete", function () {
		var row = $(this).closest("tr");
		gId = gTable.row(row).data().id;
		$("#modal-delete").modal("show");
	});

	// on btn confirm is clicked
	$("#btn-confirm").on("click", function () {
		$.ajax({
			headers: {
				Authorization: "Bearer " + gToken,
			},
			type: "DELETE",
			url: gURL + "/" + gId,
			success: function (response) {
				toastr.success("Delete successfully");
				$("#modal-delete").modal("hide");
				gTable.ajax.reload();
			},
			error: function (error) {
				toastr.error("Delete failed");
				console.log(error);
			},
		});
	});

	//on btn add is clicked
	$("#btn-add").on("click", function () {
		var vUrl = "Subscription-add.html";
		window.location.href = vUrl;
	});

	/*** REGION 3 - Event handlers */

	/*** REGION 4 - Common funtions */
}
